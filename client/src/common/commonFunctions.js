import { toast } from 'react-toastify';

const messageUser = (message) => {
  toast.dark(`${message}`, {
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });
}

export default messageUser;
