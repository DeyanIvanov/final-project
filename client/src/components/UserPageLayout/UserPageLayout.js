import { useHistory } from 'react-router';
import { useSelector } from 'react-redux';
import './UserPage.css';

const UserPageLayout = (props) => {
  const history = useHistory();
  const user = useSelector(state => state.user);

  const CUSTOMER_NAVS = [
    {
      path: '/user/new-visit',
      text: 'New visit'
    },
    {
      path: '/user/vehicles',
      text: 'Vehicles'
    },
    {
      path: '/user/history',
      text: 'Shop Visits'
    },
    {
      path: user.isLoggedIn ? `/user/${user.data.id}/profile` : '',
      text: 'Update Profile'
    },
  ];

  const EMPLOYEE_NAVS = [
    {
      path: '/user/vehicles',
      text: 'Vehicles'
    },
    {
      path: '/user/services',
      text: 'Services'
    },
    {
      path: '/user/history',
      text: 'Shop Visits'
    },
    {
      path: '/user/customers',
      text: 'Customers'
    },
    {
      path: '/user/custom-visit',
      text: 'Custom Visit'
    },
    {
      path: user.isLoggedIn ? `/user/${user.data.id}/profile` : '',
      text: 'Update Profile'
    },
  ];

  const navs = (user.isLoggedIn && user.data.role === 'customer') ? CUSTOMER_NAVS : EMPLOYEE_NAVS;

  return (
    <div className='user-page'>

      <nav className='navigation'>
        <ul>
          {navs.map(n => {
            return (
              <li key={n.text.toLowerCase().replace(' ', '-')} className='list-item' onClick={() => history.push(n.path)}>
                <p className='nav-link' to={n.path}>{n.text}</p>
              </li>
            )
          })}
        </ul>
      </nav>

      <div className='content'>
        {props.children}
      </div>

    </div>
  )
};

export default UserPageLayout;
