import logo from '../common/logo.png';

const CustomerHome = () => {
  return (
    <div>
      <h1>Welcome back!</h1>

      <img className='user-home-img' src={logo} alt='img' />
    </div>
  )
};

export default CustomerHome;
