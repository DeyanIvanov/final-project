/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import messageUser from '../../common/commonFunctions.js';
import { BASE_URL } from '../../common/constants.js';
// import { useDispatch } from 'react-redux';
// import { logout } from '../../auth/actions';

const NewVisit = () => {
  const user = useSelector(state => state.user);

  const [vehicles, setVehicles] = useState([]);
  const [services, setServices] = useState([]);

  const [vehicleSelect, setVehicleSelect] = useState('');
  const [serviceSelect, setServiceSelect] = useState('');

  useEffect(() => {
    fetch(`${BASE_URL}/api/customers/${user.data.id}/vehicles`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then(res => res.json())
      .then(res => setVehicles(res));

    fetch(`${BASE_URL}/api/customers/services`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then(res => res.json())
      .then(res => setServices(res));
  }, []);

  const handleNewVisit = () => {
    // req to server to create new visit
    fetch(`${BASE_URL}/api/customers/${user.data.id}/shop-visits`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        vehicleId: vehicleSelect,
        serviceId: serviceSelect,
        price: `${services.filter(service => +service.id === +serviceSelect)[0].price}`
      })
    })
      .then(() => { messageUser('Your order has been sent. Expect our employees to contact you soon.') });
  }

  return (
    <div>
      <h1>New Shop Visit:</h1>
      <hr className='line' />

      <div className='new-visit-container'>

        <br />
        <label className='new-visit-text' htmlFor='vehicle-select'>On which of your vehicles do you need a service?</label>
        <select
          className='custom-select new-visit-select'
          id='vehicle-select'
          onChange={(event) => { setVehicleSelect(event.target.value) }}
        >

          <option value={''}>Choose a vehicle...</option>

          {vehicles.length === 0
            ? <option value={''}>You have no vehicles.</option>
            : vehicles.map(vehicle => {
              return (
                <option
                  key={vehicle.model}
                  value={vehicle.id}
                >
                  {`${vehicle.brand} ${vehicle.model}`}
                </option>
              );
            })}
        </select>
        <br />
        <br />

        <label className='new-visit-text  ' htmlFor='vehicle-select'>What service does your vehicle need?</label>
        <select
          className='custom-select new-visit-select'
          id='service-select'
          onChange={(event) => { setServiceSelect(event.target.value) }}
        >

          <option value={''}>Choose a service...</option>

          {services.map(service =>
            <option
              key={service.service}
              value={service.id}
            >
              {service.service}
            </option>)}
        </select>

        <br />
        <br />
        <hr className='line' />

        <p className='new-visit-text new-visit-price'>
          <strong>
            {serviceSelect !== ''
              ? `Estimated Price: ${services.filter(service => +service.id === +serviceSelect)[0].price} lv.`
              : 'Estimated Price:'}
          </strong>
        </p>

        <br />
        <br />
        <br />

        <button
          disabled={(vehicleSelect !== '' && serviceSelect !== '') ? false : true}
          onClick={() => { handleNewVisit() }} className='button'
        >
          Done
        </button>
      </div>
    </div>
  )
};

export default NewVisit;
