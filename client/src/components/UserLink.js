import { useHistory } from 'react-router-dom';
import { BASE_URL } from '../common/constants';

const UserLink = (props) => {
  const history = useHistory();

  return (
    <div
      className={props.className}
      onClick={() => {
        if (!history.location.pathname.includes('/user/')) {
          history.push(`/user/${props.id}/profile`);
        } else {
          history.push(`${props.id}/profile`);
        }
      }}
    >
      <img src={`${BASE_URL}/${props.img}`} alt='avatar' className='user-link-img' />

      <p className='user-link-name'>{props.name}</p>
    </div>
  );
};

export default UserLink;