import { Link } from "react-router-dom"

const Vehicle = ({ id, name, brand, registration }) => {
  return (
    <div>
      <div>{name}</div>
      <div>{brand}</div>
      <div>{registration}</div>
      <Link to={`/user/vehicles/${id}`} >
        View Details
      </Link>
    </div>
  )
}
export default Vehicle;