import ModalUpdateService from "../Modal/ModalUpdateService";
import '../../components/Services/EmployeeService.css';
import { useState } from "react";

const EmployeeService = ({ id, service, price, deleteService, handleUpdateService }) => {
  const [showUpdateModal, setShowUpdateModal] = useState(false);

  const showModal = () => setShowUpdateModal(true);
  const closeModal = () => setShowUpdateModal(false);

  return (
    <>

      <div className="card">
        <figure className="card__thumb">
          <img src="http://www.mediakauppa.com/wp-content/uploads/2020/03/auto-repair-service.jpg" alt="Picture by Kyle Cottrell" class="card__image" />
          <figcaption className="card__caption">
            <h2 className="card__title">{service}</h2>
            <p className="card__snippet">Price: {price}</p>
            <a href="#" className="card__button" onClick={showModal}>Update</a>
            <a href="#" className="card__button" onClick={() => deleteService(id)}>Delete</a>
          </figcaption>
        </figure>
      </div>
      <ModalUpdateService
        serviceId={id}
        service={service}
        price={price}
        modalIsOpen={showUpdateModal}
        closeModal={closeModal}
        handleUpdateService={handleUpdateService} />
    </>
  )
};

export default EmployeeService;