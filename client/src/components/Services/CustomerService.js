const CustomerService = ({ service, price, priceConverted, date, endDate, status, VIN, registration }) => {

  return (
    <div className="list-card">
      <div className="list-card-main-content">
        <div>Service Name: {service}</div>
        <div>Price: {priceConverted || price}</div>
        <div>Car VIN: {VIN}</div>
        <div>Car Registration: {registration}</div>
        <div>Status: {status || 'N/A'}</div>
      </div>
      <div>{new Date(date).toLocaleString()}</div>
    </div>
  )
};

export default CustomerService;