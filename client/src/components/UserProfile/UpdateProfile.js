import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { updateName } from '../../auth/actions';
import { useParams } from 'react-router';
import { BASE_URL } from '../../common/constants';
import messageUser from '../../common/commonFunctions';

const UpdateProfileForm = () => {
  const { id } = useParams();
  const user = useSelector(state => state.user.data);
  const dispatch = useDispatch();

  const [errorMsg, setErrorMsg] = useState(null);
  const [forms, setForms] = useState({
    name: {
      placeholder: 'Enter new name...',
      value: '',
      validations: {
        required: true,
        minLength: 3,
        maxLength: 100
      },
      valid: false,
      touched: false,
      label: 'New Name:'
    },
    phone: {
      placeholder: 'Enter new phone...',
      value: '',
      validations: {
        required: true,
        format: /((\(\+359\)|0)8[789]\d{1}(|-| )\d{3}(|-| )\d{3}|08[7-9](|-| )\d{3}(|-| )\d{4})/
      },
      valid: false,
      touched: false,
      label: 'New Phone:'
    }
  });

  const [isNameFormValid, setIsNameFormValid] = useState(false);
  const [isPhoneFormValid, setIsPhoneFormValid] = useState(false);

  const handleSubmit = (event, isName) => {
    event.preventDefault()

    // build up object for the post request only with necessary data
    const data = isName ? { name: forms.name.value } : { phone: forms.phone.value };

    fetch(`${BASE_URL}/api/users/${id}/update`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify(data),
    })
      .then(r => r.json())
      .then(({ errorMessage }) => {
        if (errorMessage) {
          setErrorMsg(errorMessage);
        } else {
          messageUser('Info Updated!.');
          if (isName && user.id === +id) {
            dispatch(updateName(data.name));
          }
        }
      });
  };

  const isInputValid = (input, validations) => {
    let isValid = true;

    if (validations.required) {
      isValid = isValid && input.length !== 0;
    }
    if (validations.minLength) {
      isValid = isValid && input.length >= validations.minLength;
    }
    if (validations.maxLength) {
      isValid = isValid && input.length <= validations.maxLength;
    }
    if (validations.format) {
      isValid = isValid && validations.format.test(input);
    }

    return isValid;
  };

  const handleInputChange = (event, isName) => {
    const { name, value } = event.target;

    const updatedElement = { ...forms[name] };
    updatedElement.value = value;
    updatedElement.touched = true;
    updatedElement.valid = isInputValid(value, updatedElement.validations);

    const updatedForm = { ...forms, [name]: updatedElement }
    setForms(updatedForm);

    const formValid = (updatedForm[name].valid && updatedForm[name].touched) ? true : false;
    isName ? setIsNameFormValid(formValid) : setIsPhoneFormValid(formValid);
  }

  const formsElements = Object.keys(forms)
    .map(name => {
      return {
        id: name,
        config: forms[name]
      };
    })
    .map(({ id, config }) => {
      const isValidCSSClass = config.valid ? 'valid' : 'invalid';
      const isTouchedCSSClass = config.touched ? 'touched' : 'untouched';
      const classes = [isValidCSSClass, isTouchedCSSClass].join('-');

      return (
        <form key={id}>
          <label htmlFor={id}>{config.label}</label>
          <input
            key={id}
            name={id}
            className={`${classes} update-user-input`}
            placeholder={config.placeholder}
            value={config.value}
            onChange={(event) => {
              handleInputChange(event, id === 'name' ? true : false)
            }}
          />

          <button
            className='button'
            onClick={(event) => {
              handleSubmit(event, id === 'name' ? true : false)
            }}
            disabled={id === 'name' ? !isNameFormValid : !isPhoneFormValid}
          >Update</button>
        </form>
      );
    });

  return (
    <div>
      <p className={errorMsg ? 'errMsg' : ''}>{errorMsg}</p>

      {formsElements}
    </div>
  );
};

export default UpdateProfileForm;
