/* eslint-disable react-hooks/exhaustive-deps */
import { BASE_URL } from '../../common/constants.js';
import { useSelector, useDispatch } from 'react-redux';
import { logout, updateProfilePic } from '../../auth/actions';
import { useHistory } from 'react-router-dom'
import { useParams } from 'react-router';
import { useEffect, useState } from 'react';
import { Upload } from 'react-bootstrap-icons';
import UpdateProfileForm from './UpdateProfile.js';
import messageUser from '../../common/commonFunctions.js';

const UserProfile = () => {
  const user = useSelector(state => state.user.data);
  const history = useHistory();
  const { id } = useParams();

  // Guard:
  if (user.role === 'customer'
    && (user.role === 'employee'
      || user.id !== +id)
  ) {
    history.push('/home');
  }

  const dispatch = useDispatch();

  const [image, setImage] = useState(null);
  const [previewImage, setPreviewImage] = useState(null);
  const [isPictureUploaded, setIsPictureUploaded] = useState(false);

  const [pageUser, setPageUser] = useState({});

  useEffect(() => {
    fetch(`${BASE_URL}/api/users/${id}`, {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then(r => r.json())
      .then(newUser => {
        setPageUser(newUser)
        setPreviewImage(`${BASE_URL}/${newUser.profile_picture}`)
      })
  }, []);

  const uploadAvatar = async (event) => {
    event.preventDefault();

    const formData = new FormData()
    formData.append('image', image)

    fetch(`${BASE_URL}/api/users/${id}/img`, {
      method: 'POST',
      body: formData,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then(r => r.json())
      .then(({ image }) => {
        setPageUser({ ...pageUser, avatar: image });
        if (user.id === +id) {
          dispatch(updateProfilePic(image));
        }
      })

    messageUser('Profile Picture Uploaded!');
  };

  const handleLogout = () => {
    fetch(`${BASE_URL}/api/users/logout`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then(() => {
        localStorage.removeItem('token');

        dispatch(logout());
      });

    messageUser('Logged Out Successfully.');
  };

  return (
    <div className='update-profile-form'>

      <div className='image-frame'>
        <img className='user-photo'
          src={previewImage}
          alt='avatar'
        />
        <br />
        <br />
        <form
          className='picture-form'
          encType='multipart/form-data'
        >
          <label className='image-input-label button' htmlFor='image-input'>
            <Upload /> Upload Image
            <input
              id='image-input'
              className='image-input'
              onChange={
                (event) => {
                  if (event.target.files.length > 0) {
                    setImage(event.target.files[0]);
                    setPreviewImage(URL.createObjectURL(event.target.files[0]));
                  }

                  setIsPictureUploaded(true);
                }
              }
              name='pic'
              type='file' />
          </label>
          <br />
          <br />
          <button className='button' onClick={uploadAvatar} disabled={!isPictureUploaded}>Change</button>
        </form >
      </div>

      <UpdateProfileForm />

      <hr className='line' />
      <button id='logout-btn' className='button' onClick={handleLogout}>Log Out</button>

    </div >
  )
};

export default UserProfile;
