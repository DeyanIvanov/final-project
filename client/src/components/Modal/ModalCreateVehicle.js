import React from "react";
import { useState, useEffect } from 'react'
import { BASE_URL } from "../../common/constants";
import useHttp from "../../hooks/use-http";
import GeneralModal from "./GeneralModalForVehicles";
import "./ModalCreateVehicle.css";

const ModalCreateVehicle = ({ onVehicleCreation }) => {
  const { error, sendRequest: fetchVehicles } = useHttp();
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const showModal = () => {
    setModalIsOpen(true);
  };
  const closeModal = () => {
    setModalIsOpen(false);
  };

  const createVehicle = (vehicle) => {
    onVehicleCreation(vehicle);
    closeModal();
    // const createdTask = { name: formData.enterjkedUserName, brand: formData.brandValue, registration: formData.enteredRegistration, id: formData.id }
    // props.onVehicleCreation(createdTask)
  }

  const addVehicle = (formData) => {
    fetchVehicles({
      url: `${BASE_URL}/api/employee/vehicles`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        // 'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
      body: {
        registration: formData.enteredRegistration,
        VIN: formData.enteredVIN,
        cars_id: formData.modelValue,
        username: formData.enteredUserName,
  
      },
    }, createVehicle)
  };

  return (
    <div>
      <button className="button" onClick={showModal}>Create</button>
      <GeneralModal show={modalIsOpen}  closed={closeModal} onHandleSubmition={addVehicle} />
    </div>
  )
};

export default ModalCreateVehicle;
