import React from "react";
import { useState, useEffect } from 'react'
import { BASE_URL } from "../../common/constants";
import useHttp from "../../hooks/use-http";
import useInput from '../../hooks/use-input'
import "./ModalCreateVehicle.css";



const GeneralModal = ({ show, onHandleSubmition, service = '', price = '', closed }) => {

  const cssClasses = [
    "Modal",
    show ? "ModalOpen" : "ModalClosed"
  ];

  const {
    value: enteredPrice,
    isValid: enteredPriceIsValid,
    hasError: priceInputHasError,
    valueChangeHandler: priceChangeHandler,
    inputBlurHandler: priceBlurHandler,
  } = useInput((value) => `${value}`.trim() !== '', price);

  const {
    value: enteredService,
    isValid: enteredServiceIsValid,
    hasError: serviceInputHasError,
    valueChangeHandler: serviceChangeHandler,
    inputBlurHandler: serviceBlurHandler,
  } = useInput((value) => value.trim() !== '', service);


  let formIsValid = false;
  formIsValid = enteredPriceIsValid && enteredServiceIsValid;



  const priceInputClasses = priceInputHasError
    ? 'form-control invalid'
    : 'form-control';

  const serviceInputClasses = serviceInputHasError
    ? 'form-control invalid'
    : 'form-control';


  return (
    <div className={cssClasses.join(' ')}>
      <form onSubmit={(ev) => {
        ev.preventDefault();
        onHandleSubmition({ enteredPrice, enteredService })
      }}>
        <div className="close-btn" onClick={closed}>x</div>
        <div className={serviceInputClasses}>
          <label htmlFor='service'>Service:</label>
          <input
            autoComplete="off"
            type='text'
            id='service'
            className='modal-input'
            onChange={serviceChangeHandler}
            onBlur={serviceBlurHandler}
            value={enteredService}
          />
          {serviceInputHasError && (
            <p className='error-text'>Please enter a service.</p>
          )}
        </div>
        <div className={priceInputClasses}>
          <label htmlFor='price'>Price:</label>
          <input
            type='price'
            id='price'
            className='modal-input'
            onChange={priceChangeHandler}
            onBlur={priceBlurHandler}
            value={enteredPrice}
          />
          {priceInputHasError && (
            <p className='error-text'>Please enter a price.</p>
          )}
        </div>
        <div className='form-actions'>
          <button className="button" type="submit" disabled={!formIsValid} >Submit</button>
        </div>
      </form>
    </div>


  )
};



export default GeneralModal;