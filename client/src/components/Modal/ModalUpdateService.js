import React from "react";
import { useState, useEffect } from 'react'
import { useParams } from "react-router";
import { BASE_URL } from "../../common/constants";
import useHttp from "../../hooks/use-http";
import useInput from '../../hooks/use-input'
import GeneralModal from "./GeneralModalForServices";
import "./ModalCreateVehicle.css";
import { Link } from "react-router-dom"
import { useHistory } from 'react-router-dom';

const ModalUpdateService = ({ service, price, serviceId, handleUpdateService, modalIsOpen, closeModal }) => {
  const { sendRequest: fetchUpdateService } = useHttp();

  const updateService = (formData) => {
    fetchUpdateService({
      url: `${BASE_URL}/api/employee/services/${serviceId}`,
      method: 'PUT',
      body: {
        service: formData.enteredService,
        price: formData.enteredPrice,
      },
    }, (response) => {
      handleUpdateService(response);
      closeModal();
    })
  };

  return (
    <>
      <GeneralModal service={service} price={price} show={modalIsOpen} closed={closeModal} onHandleSubmition={updateService} />
    </>
  )
};

export default ModalUpdateService;
