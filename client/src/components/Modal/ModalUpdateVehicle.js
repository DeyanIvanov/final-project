import GeneralModal from "./GeneralModalForVehicles"
import { useEffect, useState, } from "react";
import { BASE_URL } from "../../common/constants";
import useHttp from "../../hooks/use-http";
import "./ModalCreateVehicle.css";


const ModalUpdateVehicle = ({ vehicleId, registration, VIN, name, handleUpdateVehicle }) => {
  const { sendRequest: fetchUpdateVehicle } = useHttp();
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const showModal = () => {
    setModalIsOpen(true);
  };
  const closeModal = () => {
    setModalIsOpen(false);
  };

  const updateVehicle = (formData) => {
    fetchUpdateVehicle({
      url: `${BASE_URL}/api/employee/vehicles/${vehicleId}/report`,
      method: 'PUT',
      body: {
        registration: formData.enteredRegistration,
        VIN: formData.enteredVIN,
        cars_id: formData.modelValue,
        username: formData.enteredUserName,
      },
    }, (response) => {
      closeModal();
      handleUpdateVehicle(response);
    })
  };

  return (
    <>
      <button className="button pdf-button" onClick={showModal}>Update</button>
      <GeneralModal registration={registration} VIN={VIN} name={name} show={modalIsOpen} closed={closeModal} onHandleSubmition={updateVehicle} />
    </>
  )

};

export default ModalUpdateVehicle;