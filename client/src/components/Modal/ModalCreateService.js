import React from "react";
import { useState, useEffect } from 'react'
import { BASE_URL } from "../../common/constants";
import useHttp from "../../hooks/use-http";
import useInput from '../../hooks/use-input'
import GeneralModal from "./GeneralModalForServices";
import "./ModalCreateService.css";

const ModalCreateService = ({ onServiceCreation }) => {
  const { error, sendRequest: fetchServices } = useHttp();
  const [modalIsOpen, setModalIsOpen] = useState(false);

  const showModal = () => {
    setModalIsOpen(true);
  };
  const closeModal = () => {
    setModalIsOpen(false);
  };

  const createService = (service) => {
    onServiceCreation(service);
    closeModal();
  }

  const addService = (formData) => {
    fetchServices({
      url: `${BASE_URL}/api/employee/services`,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: {
        price: formData.enteredPrice,
        service: formData.enteredService,
      },
    }, createService)
  };

  return (
    <>
      <button className="create-service-button button" onClick={showModal}>Create</button>
      <GeneralModal show={modalIsOpen} closed={closeModal} onHandleSubmition={addService} />
    </>
  )
};

export default ModalCreateService;
