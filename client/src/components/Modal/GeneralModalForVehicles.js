import React from "react";
import { useState, useEffect } from 'react'
import { BASE_URL } from "../../common/constants";
import useHttp from "../../hooks/use-http";
import useInput from '../../hooks/use-input'
import DropdownBrandsMenu from "../Dropdown/DropdownBrandsMenu";
import DropdownModelsMenu from "../Dropdown/DropdownModelsMenu";
import "./ModalCreateVehicle.css";


const GeneralModal = ({ show, onHandleSubmition, registration = '', VIN = '', name = '', closed }) => {
  const [brandValue, setBrandValue] = useState(null)
  const [brands, setBrand] = useState([])
  const [modelValue, setModelValue] = useState('')
  const [model, setModel] = useState([])

  const { errorFetchingBrands, sendRequest: fetchBrandsValues } = useHttp();
  const { errorFetchingModels, sendRequest: fetchModelsValues } = useHttp();

  useEffect(() => {
    fetchBrandsValues({ url: `${BASE_URL}/api/employee/vehicles/brands` }, (response) => {
      const options = response.map(res => res.brand);

      setBrand(options);

      setBrandValue(options[0]);
    })
  }, [fetchBrandsValues])

  useEffect(() => {
    if (!brandValue) {
      return;
    }

    fetchModelsValues({ url: `${BASE_URL}/api/employee/vehicles/${brandValue}/modelYears` }, (options) => {
      setModel(options);

      setModelValue(options[0].id);
    });
  }, [brandValue, fetchModelsValues])

  const cssClasses = [
    "Modal",
    show ? "ModalOpen" : "ModalClosed"
  ];
  const {
    value: enteredUserName,
    isValid: enteredUserNameIsValid,
    hasError: userNameInputHasError,
    valueChangeHandler: userNameChangedHandler,
    inputBlurHandler: userNameBlurHandler,
    reset: resetUserNameInput,
  } = useInput((value) => value.trim() !== '', name);

  const {
    value: enteredRegistration,
    isValid: enteredRegistrationIsValid,
    hasError: RegistrationInputHasError,
    valueChangeHandler: RegistrationChangedHandler,
    inputBlurHandler: RegistrationBlurHandler,
  } = useInput((value) => value.trim() !== '' && value.length === 8, registration);

  const {
    value: enteredVIN,
    isValid: enteredVINIsValid,
    hasError: VINInputHasError,
    valueChangeHandler: VINChangeHandler,
    inputBlurHandler: VINBlurHandler,
  } = useInput((value) => value.trim() !== '', VIN);

  let formIsValid = false;
  formIsValid = enteredRegistrationIsValid && enteredUserNameIsValid && enteredVINIsValid;

  const RegistrationInputClasses = RegistrationInputHasError
    ? 'form-control invalid'
    : 'form-control';
  const userNameInputClasses = userNameInputHasError
    ? 'form-control invalid'
    : 'form-control';

  const VINInputClasses = VINInputHasError
    ? 'form-control invalid'
    : 'form-control';

  return (
    <div className={cssClasses.join(' ')}>
      <form onSubmit={(ev) => {
        ev.preventDefault();
        onHandleSubmition({ enteredRegistration, enteredVIN, modelValue, enteredUserName })
      }}>
        <div className="close-btn" onClick={closed}>x</div>
        <div className="brand-input">
          <DropdownBrandsMenu value={brandValue} brands={brands} handleBrandChange={setBrandValue} />
        </div>
        <DropdownModelsMenu value={brandValue} model={model} handleModelChange={setModelValue} />
        <div className={RegistrationInputClasses}>
          <label htmlFor='registration'>Registration Number:</label>
          <input
            autoComplete="off"
            type='text'
            id='registration'
            className='modal-input'
            onChange={RegistrationChangedHandler}
            onBlur={RegistrationBlurHandler}
            value={enteredRegistration}
          />
          {RegistrationInputHasError && (
            <p className='error-text'>Please enter a valid registration number.</p>
          )}
        </div>

        <div className={userNameInputClasses}>
          <label htmlFor='user-name'>User Name:</label>
          <input
            autoComplete="off"
            type='text'
            id='user-name'
            className='modal-input'
            onChange={userNameChangedHandler}
            onBlur={userNameBlurHandler}
            value={enteredUserName}
          />
          {userNameInputHasError && (
            <p className='error-text'>Please enter a valid name.</p>
          )}
        </div>
        <div className={VINInputClasses}>
          <label htmlFor='vin'>VIN:</label>
          <input
            type='vin'
            id='vin'
            className='modal-input'
            onChange={VINChangeHandler}
            onBlur={VINBlurHandler}
            value={enteredVIN}
          />
          {VINInputHasError && (
            <p className='error-text'>Please enter a valid VIN.</p>
          )}
        </div>
        <div className='form-actions'>
          <button className="button" disabled={!formIsValid} >Submit</button>
        </div>
      </form>
    </div>
  )
};

export default GeneralModal;