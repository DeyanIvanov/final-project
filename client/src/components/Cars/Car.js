const Car = ({ id, brand, model, name, registation: registration, VIN, years, onCarSelected = () => {} }) => {
  return (
    <div onClick={() => onCarSelected(id)}>
      <div>{brand}</div>
      <div>{years}</div>
      <div>{model}</div>
      <div>{name}</div>
      <div>{registration}</div>
      <div>{VIN}</div>
    </div>
  )
};

export default Car;