/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { BASE_URL } from '../common/constants';
import { addToReport, removeFromReport } from '../auth/actions';
import messageUser from '../common/commonFunctions';
import { ClipboardCheck } from 'react-bootstrap-icons';
import './content.css';

const UserHistory = () => {
  const detailedReport = useSelector(state => state.pdf.detailedReport);
  const user = useSelector(state => state.user);
  const dispatch = useDispatch();

  const [shopVisits, setShopVisits] = useState([]);
  const [shopVisitsToList, setShopVisitsToList] = useState([]);

  useEffect(async () => {
    if (user.data.role === 'customer') {
      // Get list of logged customer's shop visits
      await fetch(`${BASE_URL}/api/customers/${user.data.id}/shop-visits`, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
      })
        .then(res => res.json())
        .then(res => {
          setShopVisits(res.visits);
          setShopVisitsToList(res.visits);
        })
    } else {

      // Get list of all of customers' shop visits
      await fetch(`${BASE_URL}/api/customers/shop-visits`, {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
      })
        .then(res => res.json())
        .then(res => {
          setShopVisits(res);
          setShopVisitsToList(res);
        })
    }
  }, []);

  const toggleDetailedReport = (visit) => {
    // check if already in report
    if (!detailedReport.some(shopVisitInPDF => shopVisitInPDF.id === visit.id)) {
      if (detailedReport.length > 2) {
        messageUser('You can add up to 3 visits in the report.');
      } else {
        dispatch(addToReport(visit));

        messageUser('Shop Visit Added to Detailed Report.');
      }
    } else {
      dispatch(removeFromReport(visit));

      messageUser('Shop Visit Removed from Detailed Report.');
    }
  };

  const advanceVisit = (customerId, visitId) => {
    fetch(`${BASE_URL}/api/customers/${customerId}/shop-visits/${visitId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then(() => {
        setShopVisits(shopVisits.reduce((acc, visit) => {
          if (visit.id === visitId) {
            if (visit.status === 'Not Started') {
              visit.status = 'In Progress';
              visit.start_date = new Date().toLocaleDateString('fr-CA').toString();
            } else if (visit.status === 'In Progress') {
              visit.status = 'Ready For Pickup';
              visit.end_date = new Date().toLocaleDateString('fr-CA').toString();
            }
          }
          acc.push(visit);
          return acc;
        }, []));
      })
      .then(() => { messageUser('Shop Visit Status Updated.'); });
  };

  const handleSearchChange = (value) => {
    setShopVisitsToList(shopVisits.filter(vehicle => vehicle.customer.toLowerCase().includes(value)))
  }

  return (
    <div>
      <h1 className='page-title'>Shop Visits:</h1>

      <hr className='line' />

      {
        user.data.role === 'employee'
          ? <input
            placeholder='Search Customer...'
            className='search-bar'
            onChange={(event) => { handleSearchChange(event.target.value) }}
          />
          : ''
      }

      <ul className='cars'>
        {
          shopVisitsToList.length !== 0
            ? shopVisitsToList.map(visit => {
              return (
                <li key={visit.id}>
                  < div className='history-single-shop-visit' key={visit.id}>
                    <p className='history-service'>{`${visit.service} Status: ${visit.status}`}</p>
                    <br />
                    <p className='history-vehicle'>{`Vehicle: ${visit.brand} ${visit.model} (${visit.registration})`}</p>

                    <button onClick={() => { toggleDetailedReport(visit); }} className='button  pdf-button'>
                      {detailedReport.some(shopVisitInPDF => shopVisitInPDF.id === visit.id) ? 'Remove' : 'Add to PDF'}
                    </button>

                    {user.data.role === 'employee'
                      ? <ClipboardCheck onClick={() => { advanceVisit(visit.customerId, visit.id); }} className='customer-button hssv-button' />
                      : ''
                    }

                    <br />
                    <p className='history-date'>{`Status: ${visit.status} ; Dates: ${visit.start_date === null ? 'x' : visit.start_date.split('T')[0]} - ${visit.end_date === null ? 'x' : visit.end_date.split('T')[0]}`}</p>

                  </div>
                </li>
              )
            })
            : <p className='no-results'>No Results...</p>
        }
      </ul>
    </div>
  )
};

export default UserHistory;
