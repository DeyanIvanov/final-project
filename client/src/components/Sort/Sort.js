
const Sort = ({handleSort}) => {
  
  return (
    <>
      <button className="button"  onClick={()=>{handleSort('desc')}}>Desc</button>
      <button className="button" onClick={()=>{handleSort('asc')}}>Asc</button>
    </>
  )
}
export default Sort;