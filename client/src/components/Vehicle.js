import ModalUpdateVehicle from "./Modal/ModalUpdateVehicle";

const Vehicle = ({ id, handleUpdateVehicle, name, brand, model, years, registration, VIN, includeUpdate = false }) => {
  return (
    <div className="list-card">
      <div className="list-card-main-content">
        <div>Owner: {name}</div>
        <div>Model: {brand} {model}</div>
        <div>Year: {years}</div>
        <div>Registration: {registration}; VIN: {VIN}</div>
      </div>
      {
        includeUpdate &&
        <ModalUpdateVehicle registration={registration} VIN={VIN} name={name} vehicleId={id} handleUpdateVehicle={handleUpdateVehicle}  />
      }
    </div>
  )
}
export default Vehicle;