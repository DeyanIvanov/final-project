import logo from '../common/logo.png';

const DetailedReportPDF = ({ visits }) => {
  return (
    <div id className='pdf-wrapper' style={{ textAlign: 'left', color: 'black' }}>

      <div style={{ textAlign: 'center' }}>
        <img src={logo} alt='logo' style={{ display: 'inline', position: 'absolute', top: 10, left: 20, width: 120, height: 120 }} />
        <h1 style={{ display: 'inline-block', paddingLeft: 20, fontSize: 'xx-large' }}>Snail Garage Invoice:</h1>
      </div>

      <hr style={{ width: '100%', border: '1px solid black' }} />

      <ul style={{ listStyleType: 'none', paddingRight: '40px', color: '#333' }}>
        {visits && visits.map(visit => (
          <li key={visit.id}>
            <p style={{ display: 'inline' }}>{
              `Customer Identification Number: ${visit.customerId}`
            }</p>

            <br />

            <p style={{ display: 'inline' }}>
              <strong>
                {`Name: ${visit.customer}`}
              </strong>
            </p>

            <p style={{ display: 'inline', float: 'right', margin: 0 }}>
              <strong>
                {`Email: ${visit.email}`}
              </strong>
            </p>

            <br />

            <p style={{ display: 'inline' }}>
              <strong>
                {`Vehicle: ${visit.brand} ${visit.model} ${visit.years}`}
              </strong>
            </p>

            <br />

            <p style={{ display: 'inline' }}>{
              `Registration: ${visit.registration}`
            }</p>

            <p style={{ display: 'inline', float: 'right', margin: 0 }}>
              {` VIN: ${visit.VIN}`}
            </p>

            <br />

            <p style={{ display: 'inline' }}>{
              `Shop Visit Number: ${visit.id}`
            }</p>

            <br />

            <p style={{ display: 'inline' }}>{
              `Service Done: ${visit.service} `
            }
              <strong>{`Status: ${visit.status}`}</strong>
            </p>

            <br />

            <p style={{ display: 'inline' }}>{
              `Date: ${visit.start_date? visit.start_date : 'x'} - ${visit.end_date? visit.end_date : 'x'}`
            }</p>

            <p style={{ display: 'inline', float: 'right', margin: 0 }}>
              <strong>
                {`Price: ${visit.price} lv.`}
              </strong>
            </p>

            <hr style={{ width: '100%', border: '1px solid black' }} />
          </li>
        ))}
      </ul>
    </div >
  );
};

export default DetailedReportPDF;
