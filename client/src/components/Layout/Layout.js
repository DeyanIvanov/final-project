import ReactDOM from 'react-dom';
import { useDispatch, useSelector } from 'react-redux';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import Header from './Header';
import { ToastContainer } from 'react-toastify';
import DetailedReportPDF from '../DetailedReportPDF';
import { emptyReport } from '../../auth/actions';
import 'react-toastify/dist/ReactToastify.css';

const Layout = props => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.user);
  const report = useSelector(state => state.pdf);

  // Generate PDF of all selected shop visits
  const handleDownloadPDF = async () => {
    const pdf = new jsPDF('p', 'mm', 'a4');

    await ReactDOM.render(
      <DetailedReportPDF visits={report.detailedReport} />,
      document.getElementById('pdf-container'),
    );

    const imgDataUrl = await html2canvas(
      document.getElementById('pdf-container'),
      {
        scale: 2,
        logging: false,
      },
    ).then(canvas => {
      const img = canvas.toDataURL('image/png');

      return img;
    });

    pdf.addImage(imgDataUrl, 'PNG', 0, 0, 210, (120 + (report.detailedReport.length - 1) * 80));
    pdf.save(`DetailedReport.pdf`);

    dispatch(emptyReport());
  }

  return (
    <div>
      <div style={{ left: -9000, position: 'fixed' }} id='pdf-container' />
      <Header />

      <div className="main">
        {props.children}
      </div>

      <ToastContainer />

      {
        (user.isLoggedIn && report.detailedReport.length !== 0)
          ? <button className="download-pdf" onClick={() => { handleDownloadPDF() }}>+</button>
          : ''
      }

    </div>
  )
};

export default Layout;
