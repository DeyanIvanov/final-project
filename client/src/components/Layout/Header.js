/* eslint-disable react-hooks/exhaustive-deps */
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import UserLink from '../UserLink';
import { useEffect } from 'react';
import { BASE_URL } from '../../common/constants';
import { updateName, updateProfilePic } from '../../auth/actions';
import logo from '../../common/logo.png';

const Header = () => {

  const dispatch = useDispatch();
  const user = useSelector(state => state.user);
  const history = useHistory();

  useEffect(() => {
    if (user.isLoggedIn) {
      fetch(`${BASE_URL}/api/users/${user.data.id}`, {
        headers: {
          'Authorization': `Bearer ${localStorage.getItem('token')}`,
        },
      })
        .then(res => res.json())
        .then(user => {
          dispatch(updateName(user.name));
          dispatch(updateProfilePic(user.profile_picture));
        });
    }
  }, [user.isLoggedIn]);

  return (
    <div className='header'>

      <div className='header-logo'>
        <img style={{ display: 'inline', position: 'absolute', top: 10, left: 20, width: 100, height: 100 }} src={logo} alt='logo' />
        <p onClick={() => { history.push('/home') }}>Snail Garage</p>
      </div>

      {
        user.isLoggedIn
          ? (
            <UserLink
              className='header-user header-user-link'
              id={user.data.id}
              img={user.data.profilePic}
              name={user.data.name}
            />
          )
          : <p className='header-user user-link-name header-user-link' onClick={() => { history.push('/login') }}>Sign In</p>
      }
    </div>
  )
};

export default Header;
