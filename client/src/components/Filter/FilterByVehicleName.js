import { useState } from 'react';
import { useHistory } from 'react-router-dom';

const FilterByVehicleName = ({ defaultSearch }) => {
  const [input, setInput] = useState(defaultSearch);

  const history = useHistory();

  const onChange = e => {
    setInput(e.target.value)
    history.push(`?search=${e.target.value}`)
    e.preventDefault()
  };

  return (
    <>
      <input
        key='search-bar'
        placeholder='Search...'
        className='search-bar'
        value={input}
        onChange={onChange}
      />
    </>
  )
}
export default FilterByVehicleName;