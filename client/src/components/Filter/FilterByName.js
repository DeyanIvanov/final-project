import { useEffect, useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import Vehicle from '../Vehicles/Vehicle';
import './FilterByName.css'
const FilterByName = ({ defaultSearch }) => {
  const [input, setInput] = useState(defaultSearch);
  const history = useHistory();

  const timeout = useRef();

  useEffect(() => {
    if (timeout.current) {
      clearTimeout(timeout.current);
    }

    timeout.current = setTimeout(() => {
      history.replace({
        search: input ? `search=${input}` : '',
      });
    }, 500);
  }, [input, history]);

  const onChange = e => {
    setInput(e.target.value)
  };

  return (
    <label>
      <input className="search-bar" type="text"
        key="search-bar"
        value={input}
        placeholder={"Search..."}
        onChange={onChange}
      />

    </label>
  )
}
export default FilterByName;