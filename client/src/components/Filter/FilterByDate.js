const FilterByDate = ({ handleFilterByDate }) => {

  return (
    <>
      <input className="date-input" type="date" id="date" name="date" onMouseLeave={(e) => { handleFilterByDate(e.target.value) }} />
    </>
  )
}
export default FilterByDate;