/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';
import messageUser from '../../common/commonFunctions';
import { BASE_URL } from '../../common/constants';
import '../content.css';

const registrationRegex = /^[A-Z]{1,2}\d{4}[A-Z]{2}/;

const CustomVisit = () => {
  const [errorMsg, setErrorMsg] = useState(null);

  const [cars, setCars] = useState([]);
  const [services, setServices] = useState([]);

  const [email, setEmail] = useState('');

  const [registration, setRegistration] = useState('');
  const [vin, setVin] = useState('');

  const [vehicleSelect, setVehicleSelect] = useState('');
  const [serviceSelect, setServiceSelect] = useState('');

  useEffect(() => {
    // fetch all cars
    fetch(`${BASE_URL}/api/employee/vehicles/cars`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then(res => res.json())
      .then(res => {
        setCars(res);
      });

    // fetch all services
    fetch(`${BASE_URL}/api/customers/services`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then(res => res.json())
      .then(res => setServices(res));
  }, []);

  const handleNewVisit = async () => {  // I'm apologize in advance whoever sees this

    // validate registration and vin:
    if (!registrationRegex.test(registration)) {
      setErrorMsg('Registration is invalid.');
    } else if (vin.length !== 17) {
      setErrorMsg('VIN is invalid.');
    } else {
      // register customer
      await fetch(`${BASE_URL}/api/users/register`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email })
      })
        .then(res => res.json())
        .then(async ({ errorMessage }) => {
          if (errorMessage) {
            setErrorMsg(errorMessage);
          } else {
            let id;
            let vehicleId;

            // create vehicle
            await fetch(`${BASE_URL}/api/employee/vehicles/create`, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
              },
              body: JSON.stringify({
                name: email.split('@')[0],
                registration,
                VIN: vin,
                car_id: vehicleSelect
              })
            });

            // Get the ids of the newly generated user, by getting the biggest id + 1
            await fetch(`${BASE_URL}/api/users`, {
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
              },
            })
              .then(res => res.json())
              .then(res => {
                id = res.reduce((acc, user) => {
                  if (user.id > acc) {
                    acc = user.id
                  }
                  return acc;
                }, -1);
              });

            await fetch(`${BASE_URL}/api/customers/${id}/vehicles`, {
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
              },
            })
              .then(res => res.json())
              .then(res => {
                console.log(res);
                vehicleId = `${res[0].id}`;
              });

            // create visit
            await fetch(`${BASE_URL}/api/customers/${id}/shop-visits`, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`,
              },
              body: JSON.stringify({
                vehicleId: vehicleId,
                serviceId: serviceSelect,
                price: `${services.filter(service => +service.id === +serviceSelect)[0].price}`
              })
            });

            messageUser('Visit created.')
            setErrorMsg(null);
          }
        });
    }
  };
console.log(cars);
  return (
    <div>
      <h1>New Shop Visit:</h1>
      <hr className='line' />

      <div className='new-visit-container'>
        <p className={errorMsg ? 'errMsg' : ''}>{errorMsg}</p>

        <label className='new-visit-text new-visit-price' htmlFor='email'>Enter Email:</label>
        <input className='custom-visit-input new-visit-select' id='email' placeholder='Email...' onChange={(event) => { setEmail(event.target.value); }}></input>

        <br />
        <br />

        <label className='new-visit-text new-visit-price' htmlFor='car-select'>Car:</label>
        <select
          className='custom-select new-visit-select'
          id='car-select'
          onChange={(event) => { setVehicleSelect(event.target.value) }}
        >
          <option value={''}>Choose a car...</option>

          {cars.map(car =>
            <option
              key={car.id}
              value={car.id}
            >
              {`${car.brand} ${car.model}`}
            </option>)}
        </select>

        <br />
        <br />

        <label className='new-visit-text new-visit-price' htmlFor='registration-input'>Registration:</label>
        <input className='custom-visit-input new-visit-select' id='registration-input' placeholder='Registration...' onChange={(event) => { setRegistration(event.target.value); }}></input>

        <br />
        <br />

        <label className='new-visit-text new-visit-price' htmlFor='VIN-input'>VIN:</label>
        <input className='custom-visit-input new-visit-select' id='VIN-input' placeholder='VIN...' onChange={(event) => { setVin(event.target.value); }}></input>

        <br />
        <br />

        <label className='new-visit-text new-visit-price' htmlFor='vehicle-select'>Choose a service...</label>

        <select
          className='custom-select new-visit-select'
          id='service-select'
          onChange={(event) => { setServiceSelect(event.target.value) }}
        >

          <option value={''}>Choose a service...</option>

          {services.map(service =>
            <option
              key={service.service}
              value={service.id}
            >
              {service.service}
            </option>)}
        </select>

        <br />
        <br />
        <hr className='line' />

        <p className='new-visit-text new-visit-price'>
          <strong>
            {serviceSelect !== ''
              ? `Estimated Price: ${services.filter(service => +service.id === +serviceSelect)[0].price}`
              : 'Estimated Price:'}
          </strong>
        </p>

        <br />
        <br />
        <br />

        <button
          disabled={(vehicleSelect !== '' && serviceSelect !== '') ? false : true}
          onClick={() => { handleNewVisit() }} className='button'
        >
          Done
      </button>
      </div>
    </div>
  )
};

export default CustomVisit;
