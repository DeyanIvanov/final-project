/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { addToReport, removeFromReport } from '../../auth/actions';
import { BASE_URL } from '../../common/constants.js';
import { TrashFill, PersonXFill, PersonCheckFill, ArrowDownCircle, ArrowUpCircle, CaretDownSquareFill, CaretUpSquareFill } from 'react-bootstrap-icons';
import UserLink from '../UserLink';
import messageUser from '../../common/commonFunctions';
import '../content.css';

const Customers = () => {
  const detailedReport = useSelector(state => state.pdf.detailedReport);
  const dispatch = useDispatch();

  const [sortBy, setSortBy] = useState('name');
  const [order, setOrder] = useState('asc');

  const [customers, setCustomers] = useState([]);

  useEffect(async () => {
    let partialCustomers;

    // Get list off customers
    await fetch(`${BASE_URL}/api/users`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then(res => res.json())
      .then(res => {
        partialCustomers = res.filter(customer => customer.role === 'customer');
      })

    partialCustomers = partialCustomers.map(customer => {
      return { ...customer, shopVisits: [], areVisitsVisible: false }
    });

    // Get list of all of customers' shop visits
    await fetch(`${BASE_URL}/api/customers/shop-visits`, {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then(res => res.json())
      .then(res => {
        res.map(shopVisit => {
          partialCustomers.map(customer => {
            if (customer.id === shopVisit.customerId) {
              customer.shopVisits.push(shopVisit);
            }
          })
        })
      })

    setCustomers(partialCustomers);
  }, []);

  const handleDeleteButton = (id) => {
    fetch(`${BASE_URL}/api/users/${id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then(() => {
        messageUser('Customer Deleted!');
      });

    setCustomers(customers.filter(customer => customer.id !== id));
  };

  const handleBanButton = (id) => {
    fetch(`${BASE_URL}/api/users/${id}/ban`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then(() => {
        messageUser('Customer Banned!');
      });

    setCustomers(customers.reduce((acc, customer) => {
      if (customer.id === id) {
        acc.push({ ...customer, is_banned: 1 })
      } else {
        acc.push(customer);
      }
      return acc;
    }, []));
  };

  const handleUnbanButton = (id) => {
    fetch(`${BASE_URL}/api/users/${id}/unban`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then(() => {
        messageUser('Customer Unbanned!');
      });

    setCustomers(customers.reduce((acc, customer) => {
      if (customer.id === id) {
        acc.push({ ...customer, is_banned: 0 })
      } else {
        acc.push(customer);
      }
      return acc;
    }, []));
  };

  const toggleDetailedReport = (visit) => {
    // check if already in report
    if (!detailedReport.some(shopVisitInPDF => shopVisitInPDF.id === visit.id)) {
      if (detailedReport.length > 2) {
        messageUser('You can add up to 3 visits in the report.');
      } else {
        dispatch(addToReport(visit));

        messageUser('Shop Visit Added to Detailed Report.');
      }
    } else {
      dispatch(removeFromReport(visit));

      messageUser('Shop Visit Removed from Detailed Report.');
    }
  };

  const collapseVisits = (customer) => {
    setCustomers(customers.reduce((acc, c) => {
      if (c.id === customer.id) {
        c.areVisitsVisible = !c.areVisitsVisible;
      }
      acc.push(c);
      return acc;
    }, []));
  }

  const reorder = () => {
    if (order === 'asc') {
      setCustomers(customers.sort((firstElem, secondElem) => {
        const first = firstElem[sortBy].toLowerCase();
        const second = secondElem[sortBy].toLowerCase();
        if (first < second) {
          return -1;
        }
        if (first > second) {
          return 1;
        }
      }));
    } else {
      setCustomers(customers.sort((firstElem, secondElem) => {
        const first = firstElem[sortBy].toLowerCase();
        const second = secondElem[sortBy].toLowerCase();
        if (first < second) {
          return 1;
        }
        if (first > second) {
          return -1;
        }
      }));
    }
  }

  return (
    <div>
      <h1 className='page-title'>Customers:</h1>

      {
        order === 'asc'
          ? <ArrowUpCircle className='sort-customer-button' onClick={() => {
            setOrder('desc');
            reorder()
          }} />
          : <ArrowDownCircle className='sort-customer-button' onClick={() => {
            setOrder('asc');
            reorder();
          }} />
      }

      <select
        id='sort-select'
        className='sort-select custom-select-customers'
        onChange={(event) => {
          setSortBy(event.target.value);
          reorder();
        }}
      >
        <option value='name'>Name</option>
        <option value='email'>Email</option>
      </select>
      <label htmlFor='sort-select' className='sort-select'><strong>Order By:</strong></label>

      <hr className='line' />

      <ul className='cars'>
        {
          customers.map(customer =>
            <div key={customer.name}>
              <li key={customer.name} className='customer'>
                <div>
                  <UserLink className='name' img={customer.profile_picture} id={customer.id} name={customer.name} />
                  <TrashFill onClick={() => { handleDeleteButton(customer.id); }} className='customer-button' />

                  {customer.is_banned === 1
                    ? <PersonCheckFill onClick={() => { handleUnbanButton(customer.id); }} className='customer-button' />
                    : <PersonXFill onClick={() => { handleBanButton(customer.id); }} className='customer-button' />
                  }

                  {customer.areVisitsVisible === false
                    ? <CaretDownSquareFill onClick={() => { collapseVisits(customer); }} className='customer-button' />
                    : <CaretUpSquareFill onClick={() => { collapseVisits(customer); }} className='customer-button' />
                  }

                  <br />
                  <p className='email'>{`Email: ${customer.email}`}</p>

                  <p className='phone'>{customer.phone ? `Phone Number: ${customer.phone}` : 'Phone Number: not entered'}</p>
                </div>

              </li>

              {customer.areVisitsVisible
                ? <div className='shop-visits'>
                  <p className='name'>Shop Visits:</p>
                  <hr className='line' />
                  {
                    customer.shopVisits.length !== 0
                      ? customer.shopVisits.map(visit => {
                        return (
                          <div key={visit.id}>
                            < div className='single-shop-visit' key={visit.id}>
                              <p className='ssv-service'>{`${visit.service} - ${visit.brand} ${visit.model} (${visit.registration}) - ${visit.status}`}</p>
                              <button onClick={() => { toggleDetailedReport(visit); }} className='button  pdf-button'>
                                {detailedReport.some(shopVisitInPDF => shopVisitInPDF.id === visit.id) ? 'Remove' : 'Add to PDF'}
                              </button>
                            </div>
                            <br />
                          </div>
                        )
                      })
                      : <p className='ssv-service'>Customer hasn't visited the shop yet.</p>
                  }
                </div>
                : ''
              }
            </div>
          )
        }
      </ul>
    </div >
  )
};

export default Customers;
