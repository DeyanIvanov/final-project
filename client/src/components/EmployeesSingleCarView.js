/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { BASE_URL } from '../common/constants';
import Car from './Cars/Car';
import ModalUpdateVehicle from './Modal/ModalUpdateVehicle';
import useHttp from '../hooks/use-http';

const EmployeeSingleVehicleView = () => {
  const { id } = useParams();

  const [vehicle, setVehicle] = useState({});
  const { error, loading, sendRequest: fetchSingleVehicle } = useHttp();

  useEffect(() => {
    fetchSingleVehicle({ url: `${BASE_URL}/api/employee/vehicles/${id}/report` }, setVehicle)
  }, [id]);

  const handleUpdateVehicle = ({ name, brand, model, registration, VIN }) => {
    setVehicle(prevVehicleData => ({
      ...prevVehicleData,
      name,
      brand,
      model,
      registration,
      VIN,
    }));
  };

  if (loading) {
    return null;
  }

  if (error) {
    return 'We were unable to load vehicle data! Please try again.';
  }

  return (
    <>
      <Car {...vehicle}  />
      <ModalUpdateVehicle handleUpdateVehicle={handleUpdateVehicle} vehicleId={id} properties={vehicle} />
    </>
  )
};

export default EmployeeSingleVehicleView;