import './RangePriceSlider.css'

const RangePriceSlider = () =>{
  // $('#range').on("input", function() {
  //   $('.output').val(this.value +",000  $" );
  //   }).trigger("change");

  return(
    <div className="price-range-container">
      <p class="budget">What is your budget?</p>
<label for="range">
      <input type="range" name="range" id="range" min="0" max="300" step="5" value="175"/>
</label>
 <output for="range" class="output"></output>
    </div>
  )
};
export default RangePriceSlider;