const DropdownCurrencyMenu = ({ currency, handleCurrencyChange, value }) => {
  return (
    <>
      <form action="/action_page.php">
        <label for="cars">Choose a currency:</label>
        <select onChange={(ev) => handleCurrencyChange(ev.target.value)} name="cars" id="cars"  >
          {currency.map(c => (<option selected={c === value} value={c}>{c}</option>))}
        </select>
        <br></br>
      </form>
    </>
  )
}
export default DropdownCurrencyMenu