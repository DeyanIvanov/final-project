const DropdownModelsMenu = ({ model, handleModelChange, value }) => {
  return (
    <>
      <form>
        <label for="cars">Choose a Model:</label>
        <select onChange={(ev) => handleModelChange(ev.target.value)} name="model" id="model"  >
          {model.map(m => (<option selected={m.id === value} value={m.id}>{m.model} - {m.years}</option>))}
        </select>
        <br></br>
      </form>
    </>
  )
}
export default DropdownModelsMenu;