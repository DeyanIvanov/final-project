const DropdownBrandsMenu = ({ brands, handleBrandChange, value }) => {
  return (
    <>
      <div>
        <label for="cars">Choose a brand:</label>
        <select onChange={(ev) => handleBrandChange(ev.target.value)} name="brand" id="brand"  >
          {brands.map(b => (<option selected={b === value} value={b} >{b}</option>))}
        </select>
        <br></br>
      </div>
    </>
  )
}
export default DropdownBrandsMenu;