// /* eslint-disable react-hooks/exhaustive-deps */
// import { useEffect, useState, } from 'react';
// import { BASE_URL } from '../common/constants';
// import { useSelector } from 'react-redux';
// import Vehicle from './Vehicle';
// import ModalCreateVehicle from './Modal/ModalCreateVehicle';

// const UserVehicles = () => {
//   const user = useSelector(state => state.user);

//   const [vehicles, setVehicles] = useState([]);
//   const [vehiclesToList, setVehiclesToList] = useState([]);

//   useEffect(() => {
//     if (user.data.role === 'employee') {
//       fetch(`${BASE_URL}/api/employee/vehicles`, {
//         headers: {
//           'Content-Type': 'application/json',
//           'Authorization': `Bearer ${localStorage.getItem('token')}`
//         }
//       })
//         .then(res => res.json())
//         .then(res => {
//           setVehicles(res);
//           setVehiclesToList(res);
//         });
//     } else {
//       fetch(`${BASE_URL}/api/customers/${user.data.id}/vehicles`, {
//         headers: {
//           'Content-Type': 'application/json',
//           'Authorization': `Bearer ${localStorage.getItem('token')}`
//         }
//       })
//         .then(res => res.json())
//         .then(res => {
//           setVehicles(res);
//           setVehiclesToList(res);
//         });
//     }
//   }, [])

//   const handleSearchChange = (value) => {
//     setVehiclesToList(vehicles.filter(vehicle => vehicle.name.toLowerCase().includes(value)))
//   }

//   const handleVehicleCreation = (vehicle) => {

//     // make fetch that creates vehicle
//     setVehicles(prevVehicles => {
//       return [...prevVehicles, vehicle]
//     })
//   }
// console.log(vehiclesToList, 'list')

//   return (
//     <>
//       <h1>Vehicles:</h1>

//       <hr className='line' />

//       <ModalCreateVehicle onVehicleCreation={handleVehicleCreation} />

//       {
//         user.data.role === 'employee'
//           ? <input
//             key='search-bar'
//             placeholder='Search...'
//             className='search-bar'
//             onChange={(event) => { handleSearchChange(event.target.value) }}
//           />
//           : ''
//       }

//       {vehiclesToList.map(vehicle => <Vehicle {...vehicle} key={vehicle.id} />)}
//     </>
//   )
// };
// export default UserVehicles
