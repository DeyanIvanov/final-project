import { Redirect, Route } from 'react-router-dom'

const GuardedRoute = ({ component: Component, condition, ...rest }) => {
  return (
    <Route {...rest} render={(props) => condition ? <Component {...props} /> : <Redirect to="/home" />} />
  )
};

export default GuardedRoute;
