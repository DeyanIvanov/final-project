/* eslint-disable default-case */
const initialState = {
  detailedReport: []
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'ADD_TO_REPORT':
      return {
        ...state,
        detailedReport: [...state.detailedReport, action.payload]
      };

    case 'REMOVE_FROM_REPORT':
      return {
        ...state,
        detailedReport: [
          ...state.detailedReport.filter(shopVisit => shopVisit.id !== action.payload)
        ]
      };

    case 'EMPTY_REPORT':
      return {
        ...state,
        detailedReport: []
      };

    default:
      return state;
  };
};

export default userReducer;
