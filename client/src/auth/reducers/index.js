import { combineReducers } from 'redux';
import userReducer from './user.js';
import detailedReportReducer from './detailedReport.js';

const allReducers = combineReducers({
  user: userReducer,
  pdf: detailedReportReducer,
});

export default allReducers;
