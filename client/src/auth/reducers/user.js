import decode from 'jwt-decode'

const getUser = () => {
  try {
    return decode(localStorage.getItem('token') || '');
  } catch (error) {
    return null;
  }
};

const initialState = {
  isLoggedIn: Boolean(getUser()),
  data: getUser(),
}

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        isLoggedIn: true,
        data: action.payload,
      };

    case 'LOGOUT':
      return {
        isLoggedIn: false,
        data: null,
      };

    case 'UPDATE_NAME':
      return {
        ...state,
        data: { ...state.data, name: action.payload },
      };

      case 'UPDATE_PROFILE_PIC':
      return {
        ...state,
        data: { ...state.data, profilePic: action.payload },
      };

    default:
      return state;
  };
};

export default userReducer;
