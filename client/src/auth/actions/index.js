export const login = (newUser) => {
  return {
    type: 'LOGIN',
    payload: newUser,
  };
};

export const logout = () => {
  return {
    type: 'LOGOUT',
  };
};

export const updateName = (newName) => {
  return {
    type: 'UPDATE_NAME',
    payload: newName
  };
};

export const updateProfilePic = (newPic) => {
  return {
    type: 'UPDATE_PROFILE_PIC',
    payload: newPic
  };
};

export const addToReport = (shopVisit) => {
  return {
    type: 'ADD_TO_REPORT',
    payload: shopVisit,
  };
};

export const removeFromReport = (shopVisit) => {
  return {
    type: 'REMOVE_FROM_REPORT',
    payload: shopVisit.id,
  };
};

export const emptyReport = () => {
  return {
    type: 'EMPTY_REPORT'
  };
};
