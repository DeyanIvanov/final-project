import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import GuardedRoute from './auth/GuardedRoute';
import './App.css';

// Layouts
import Layout from './components/Layout/Layout';
import UserPageLayout from './components/UserPageLayout/UserPageLayout';

// Components
import UserHome from './components/UserHome';
import UserVehicles from './components/UserVehicles';
import UserHistory from './components/UserHistory';
import UserProfile from './components/UserProfile/UserProfile';
import CustomerNewVisit from './components/CustomerComponents/NewVisit';
import EmployeeServices from './pages/EmployeesServices/EmployeesServices';
import EmployeeCustomers from './components/EmployeeComponents/Customers';
import EmployeeCustomVisit from './components/EmployeeComponents/CustomVisit';

import SingleVehicle from './components/EmployeesSingleCarView';

// Pages
import Home from './pages/Home/Home';
import LoginPage from './pages/AuthPages/LoginPage';
import RegisterPage from './pages/AuthPages/RegisterPage';
import ForgotPasswordPage from './pages/AuthPages/ForgotPasswordPage';
import ResetPasswordPage from './pages/AuthPages/ResetPasswordPage';
import NotFount from './pages/NotFound/NotFound';
import EmployeeVehiclesView from './pages/EmployeeVehicles/EmployeeVehiclesView';
import EmployeeSingleVehicleView from './pages/EmployeesSingleCarView/EmployeesSingleCarView';
import ModalUpdateService from './components/Modal/ModalUpdateService';
import CustomerCars from './pages/CustomersCars/CustomerCars';
import CustomerServices from './pages/CustomerServices/CustomerServices';
import ServicesDetailedReport from './pages/ServicesDetailedReport/ServicesDetailedReport';


const App = () => {
  const user = useSelector(state => state.user);

  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          <Redirect path='/' exact to='/home' />
          <Route path='/home' exact component={Home} />

          {/* Login Routes: */}
          <GuardedRoute path='/login' exact condition={!user.isLoggedIn} component={LoginPage} />
          <GuardedRoute path='/register' exact condition={!user.isLoggedIn} component={RegisterPage} />
          <GuardedRoute path='/forgot' exact condition={!user.isLoggedIn} component={ForgotPasswordPage} />
          <GuardedRoute path='/reset' exact condition={!user.isLoggedIn} component={ResetPasswordPage} />

          {/* User Page: */}
          <UserPageLayout>
            <GuardedRoute path='/user' exact condition={user.isLoggedIn} component={UserHome} />
            {/* <GuardedRoute path='/user/vehicles' exact condition={user.isLoggedIn} component={UserVehicles} /> */}
            {/* <GuardedRoute path='/user/vehicles/:id' exact condition={user.isLoggedIn} component={SingleVehicle} /> */}
            <GuardedRoute path='/user/history' exact condition={user.isLoggedIn} component={UserHistory} />
            <GuardedRoute path='/user/:id/profile' exact condition={user.isLoggedIn} component={UserProfile} />
            <Route path='/user/vehicles' exact render={() => {
              if (!user.isLoggedIn) {
                return <Redirect to="/home" />
              }

              if (user.data.role === 'customer') {
                return <CustomerCars />;
              } else {
                return <EmployeeVehiclesView />
              }
            }} />

            {/* Customer Specific Routes: */}
            <GuardedRoute
              path='/user/new-visit' exact
              condition={user.isLoggedIn && user.data.role === 'customer'}
              component={CustomerNewVisit}
            />
            {/* <GuardedRoute
              path='/user/vehicles'
              exact
              condition={user.isLoggedIn && user.data.role === 'customer'}
              component={CustomerCars}
            /> */}
            {/* <Route
              path='/user/vehicles/:id' exact
              render={() => {
                if (!user.isLoggedIn) {
                  return <Redirect to="/home" />
                }

                console.log(user.data.role, 'role', user.data.role === 'customer')
                if (user.data.role === 'çustomer') {
                  return <CustomerServices />;
                }

               
                return <EmployeeSingleVehicleView />
              }}
            /> */}
            <GuardedRoute path='/customer/vehicles/:id' exact condition={user.isLoggedIn && user.data.role === 'customer'} component={CustomerServices} />
            <GuardedRoute path='/user/vehicles/:id' exact condition={user.isLoggedIn && user.data.role === 'employee'} component={EmployeeSingleVehicleView} />
            <GuardedRoute
              path='/user/vehicle/:id/detailed' exact
              condition={user.isLoggedIn && user.data.role === 'customer'}
              //CustomerCars
              component={ServicesDetailedReport}
            />

            {/* Employee Routes: */}
            {/* <GuardedRoute
              path='/user/vehicles'
              exact
              condition={user.isLoggedIn && user.data.role === 'employee'}
              component={EmployeeVehiclesView}
            /> */}
            {/* Employee Specific Routes: */}
            <Route
              path='/user/services' exact
              condition={user.isLoggedIn && user.data.role === 'employee'}
              component={EmployeeServices}
            />
            <Route
              path='/user/services/:id' exact
              condition={user.isLoggedIn && user.data.role === 'employee'}
              component={ModalUpdateService}
            />
            <GuardedRoute
              path='/user/customers' exact
              condition={user.isLoggedIn && user.data.role === 'employee'}
              component={EmployeeCustomers}
            />
            <GuardedRoute
              path='/user/custom-visit' exact
              condition={user.isLoggedIn && user.data.role === 'employee'}
              component={EmployeeCustomVisit}
            />
          </UserPageLayout>

          <Route path='*' component={NotFount} />
        </Switch>
      </Layout>
    </BrowserRouter>

  );



};

export default App;
