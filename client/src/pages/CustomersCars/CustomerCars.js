import { useEffect, useState, } from "react";
import { BASE_URL } from "../../common/constants";
import useHttp from "../../hooks/use-http";
import Car from "../../components/Cars/Car";
import { useHistory } from "react-router";
import Vehicle from "../../components/Vehicle";

const CustomerCars = () => {
  const [vehicles, setVehicles] = useState([]);
  const history = useHistory();
  // const params=useParams();

  //isLoading
  const { error, sendRequest: fetchCars } = useHttp();

  useEffect(() => {
    // TODO remove id and get it from token on the BE
    fetchCars({ url: `${BASE_URL}/api/customers/cars` }, setVehicles)
  }, [fetchCars])

  const handleCarSelected = (carId) => {
    history.push(`/user/vehicles/${carId}`);
  }

  console.log('cars', vehicles)

  return (
    <>
      <div className="page-content">
        <h1 className="page-title">My Vehicles</h1>
        <hr className="line" />
        {vehicles.map(v => <div style={{ cursor: 'pointer'}} onClick={() => {
          history.push(`/customer/vehicles/${v.id}`)
        }}><Vehicle id={v.vehicleId} key={v.id} {...v} /> </div>)}
      </div>
    </>
  )
};

export default CustomerCars;