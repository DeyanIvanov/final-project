
import { useState } from 'react';
import { useSelector } from 'react-redux';
import img from '../../common/home-pic.jpg';

const Home = (props) => {
  const [pageToShow, setPageToShow] = useState('home');

  const user = useSelector(state => state.user);

  return (
    <div id='home-page'>

      <br />
      <br />

      <ul className='home-navs'>
        <li className='home-navs'>
          <h1
            className={`home-link ${pageToShow === 'home' ? 'home-link-selected' : ''}`}
            onClick={() => { setPageToShow('home') }}
          >Home</h1>
        </li>

        <li className='home-navs'>
          <h1
            className={`home-link ${pageToShow === 'about' ? 'home-link-selected' : ''}`}
            onClick={() => { setPageToShow('about') }}
          >About</h1>
        </li>

        <li className='home-navs'>
          <h1
            className={`home-link ${pageToShow === 'contacts' ? 'home-link-selected' : ''}`}
            onClick={() => { setPageToShow('contacts') }}
          >Contacts</h1>
        </li>
      </ul>

      <br />

      {pageToShow === 'home'
        ? (
          <div>

            <img className='home-pic' src={img} alt='img' />

            <div className='home-content'>
              Welcome to Snail Garage
              <br />
              <br />
              We are a not only a garage, but a car service that offers a wide range of premium parts and quality workers. Ready to start driving the car of your dreams? Simply come visit our shop and we will make sure to make that dream come true.
              <br />
              <br />
              This is student project, part of the final assignment of the Telerik Academy A27 JavaScript track.
              <br />
              <br />
              {
                user.isLoggedIn
                  ? <button className='button' onClick={() => props.history.push('/user')}>Profile</button>
                  : <button className='button' onClick={() => props.history.push('/login')}>Sign in</button>
              }
            </div>
          </div >
        )
        : ''
      }

      {pageToShow === 'about'
        ? (
          <div className='about-content'>
            <h1>About Us:</h1>
            <ul className='about-person'>
              <li>
                <div className='person'>
                  <h2 style={{ display: 'inline' }}>Deyan</h2>
                  <br />
                  <br />
                  <br />
                  <br />
                  <p style={{ display: 'inline' }}>Hello there, I'm Deyan, or as I like to be called - Dido. I'm just a 19 years old kid that likes to push himself.</p>
                </div>
              </li>

              <li>
                <div className='person'>
                  <h2 style={{ display: 'inline' }}>Dayana</h2>
                  <br />
                  <br />
                  <br />
                  <br />
                  <p style={{ display: 'inline' }}> Hello, I am Dayana, 21 years old. Currently studying Bussines Administration. I am interested in design and music.</p>
                </div>
              </li>
            </ul>
          </div >
        )
        : ''
      }

      {pageToShow === 'contacts'
        ? (
          <div className='about-content'>
            <p className='contact-us-invitation'>
              If you need to get in touch with our team - just write us quick message and we'll get in touch with you in no time.
            </p>
            <form className='contact-page-form'>
              <input className='custom-visit-input' placeholder='Your name..' />
              <br />
              <input className='custom-visit-input' placeholder='Your email..' />
              <br />
              <br />
              <textarea className='contact-us-text' placeholder='Write something..'></textarea>
            </form>
            <br />
            <button className='button'>Submit</button>
          </div>
        )
        : ''
      }

      <br />

    </div>
  );
};

export default Home;
