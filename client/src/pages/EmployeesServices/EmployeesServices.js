import { useEffect, useState } from "react";
import { BASE_URL } from "../../common/constants";
import ModalCreateService from "../../components/Modal/ModalCreateService";
import EmployeeService from '../../components/Services/EmployeeService';
import useHttp from "../../hooks/use-http";
import { useMemo } from 'react';
import { useLocation, useParams } from 'react-router';
import FilterByName from "../../components/Filter/FilterByName";
import './EmployeesServices.css';

const EmployeeServices = () => {
  const [services, setServices] = useState([]);
  const { error, loading, sendRequest: fetchServices } = useHttp();
  const { errorDeleteService, loadingDeletion, sendRequest: fetchDeleteService } = useHttp();
  const { errorFetchByName, sendRequest: fetchServicesByName } = useHttp();

  const handleUpdateService = ({ service, price, id }) => {
    const serviceToUpdate = {
      id: id,
      service: service,
      price: price
    };

    setServices(prevServices => {
      const updatedServices = prevServices.map(s => {
        if (s.id === id) {
          return {
            ...s,
            ...serviceToUpdate
          };
        }

        return s;
      })

      return updatedServices;
    });
  };

  useEffect(() => {
    fetchServices({ url: `${BASE_URL}/api/employee/services` }, setServices)
  }, [], handleUpdateService);

  const location = useLocation();
  const { search, url } = useMemo(() => {
    let search = '';
    let url = `${BASE_URL}/api/employee/services`
    if (location.search) {
      search = location.search.slice(8);
      fetchServicesByName({ url: `${BASE_URL}/api/employee/services?search=${search}` }, setServices)
    }

    return { url, search };
  }, [location.search])


  const handleServiceDelete = service => {
    const serviceId = service.id
    setServices(prevServices => {
      return prevServices.filter(service => service.id !== serviceId)
    })
  };

  const deleteService = (id) => {

    fetchDeleteService({
      //to pass real id:
      url: `${BASE_URL}/api/employee/services/${id}`,
      method: 'DELETE',
    }, handleServiceDelete)
  }

  if (loading) {
    return null;
  }

  if (error) {
    return 'We were unable to load services data! Please try again.';
  }

  const handleServiceCreation = (service) => {
    setServices(prevServices => {
      return [...prevServices, service]
    })
  }

  return (
    <div className="employee-services-container">
      <h1>Services</h1>
      <FilterByName defaultSearch={search} />
      <ModalCreateService onServiceCreation={handleServiceCreation} />

      <div className='container' >
        {services.map(service => <EmployeeService id={service.id} deleteService={deleteService} handleUpdateService={handleUpdateService} {...service} id={service.id} />)}
      </div>
    </div>
  )
};

export default EmployeeServices;