import { useEffect, useState, } from "react";
import { BASE_URL } from "../../common/constants";
import useHttp from "../../hooks/use-http";
import Sort from "../../components/Sort/Sort";
import FilterByDate from "../../components/Filter/FilterByDate";
import { Link, NavLink, useParams } from "react-router-dom";
import CurrencyButton from "../../common/currencyButton/CurrencyButton";
import CustomerService from "../../components/Services/CustomerService";

import './CustomerServices.css';

const CustomerServices = ({ }) => {
  const { id } = useParams();
  const [services, setServices] = useState([]);
  const [sort, setSort] = useState('asc');
  const [from, setFrom] = useState(null);
  const [to, setTo] = useState(null);

  const { error, sendRequest: fetchServices } = useHttp();

  useEffect(() => {
    const url = `${BASE_URL}/api/customers/cars/${id}/services`;

    let query = `sort=${sort}`;
    if (to) {
      query += `&from=${from}&to=${to}`
    } else if (from) {
      query += `&from=${from}`
    };

    fetchServices({ url: `${url}?${query}` }, setServices)
  }, [fetchServices, sort, from, to, id])

  return (
    <>
      <div className="page-content">
        <h1 className="page-title">Services</h1>
        <hr className="line" />

        <Sort handleSort={setSort} />
        <div className="date-filters">
          <span>From:</span>
          <FilterByDate handleFilterByDate={setFrom} />
          <span>To:</span>
          <FilterByDate handleFilterByDate={setTo} />
        </div>

        {services.map(service => <CustomerService {...service} key={service.id} />)}
      </div>
    </>
  )
};

export default CustomerServices;