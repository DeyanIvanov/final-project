import { useEffect, useState, } from "react";
import { BASE_URL } from "../../common/constants";
import FilterByVehicleName from "../../components/Filter/FilterByName";
import Vehicle from "../../components/Vehicle";
import useHttp from "../../hooks/use-http";
import { useMemo } from 'react';
import { useLocation } from 'react-router';
import ModalCreateVehicle from "../../components/Modal/ModalCreateVehicle";

const EmployeeVehiclesView = ({ enteredRegistration, enteredVIN, brandValue }) => {
  const [vehicles, setVehicles] = useState([]);

  // isLoading
  const { sendRequest: fetchVehicles } = useHttp();

  const { errorFetchByName, sendRequest: fetchVehiclesByName } = useHttp();

  const location = useLocation();

  useEffect(() => {
    if (location.search) {
      const search = location.search.slice(8);
      fetchVehiclesByName({ url: `${BASE_URL}/api/employee/vehicles?search=${search}` }, setVehicles)
    } else {
      fetchVehicles({ url: `${BASE_URL}/api/employee/vehicles` }, setVehicles)
    }
  }, [location.search])

  const handleVehicleCreation = (vehicle) => {
    setVehicles(prevVehicles => {
      return [...prevVehicles, vehicle]
    })
  }

  const handleUpdateVehicle = (updatedVehicle) => {
    setVehicles((prevVehicles) => prevVehicles.map(v => v.vehicleId === updatedVehicle.vehicleId ? updatedVehicle : v));
  };

  return (
    <div>
      <h1 className="page-title">Vehicles</h1>
      <hr className="line" />
      <FilterByVehicleName defaultSearch={location.search.slice(8)} />
      <div className="create-vehicle-container">
        <ModalCreateVehicle onVehicleCreation={handleVehicleCreation} />
      </div>
      {vehicles.map(vehicle => <Vehicle handleUpdateVehicle={handleUpdateVehicle} includeUpdate {...vehicle} key={vehicle.vehicleId} />)}
    </div>
  )
};
export default EmployeeVehiclesView
