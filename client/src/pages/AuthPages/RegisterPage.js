import { useState } from 'react';
import { BASE_URL } from '../../common/constants.js';
import { Link } from 'react-router-dom';
import './RegisterLoginForms.css'
import messageUser from '../../common/commonFunctions.js';

const RegisterPage = (props) => {
  const [errorMsg, setErrorMsg] = useState(null);
  const [isFormValid, setIsFormValid] = useState(false);
  const [form, setForm] = useState({
    email: {
      placeholder: 'Enter email...',
      value: '',
      validations: {
        required: true,
        format: /^[\w.]+@([\w-]+\.)+[\w-]{2,4}$/
      },
      valid: false,
      touched: false
    },
  });

  const handleInputChange = event => {
    const { name, value } = event.target;

    const updatedElement = { ...form[name] };
    updatedElement.value = value;
    updatedElement.touched = true;
    updatedElement.valid = isInputValid(value, updatedElement.validations);

    const updatedForm = { ...form, [name]: updatedElement }
    setForm(updatedForm);

    const formValid = Object.values(updatedForm).every(elem => elem.valid);
    setIsFormValid(formValid);
  }

  const handleSubmit = event => {
    // do not submit defaultly
    event.preventDefault();

    // build up object for the post request only with necessary data
    const data = Object.keys(form)
      .reduce((acc, elementKey) => {
        return {
          ...acc,
          [elementKey]: form[elementKey].value
        }
      }, {});

    // register
    fetch(`${BASE_URL}/api/users/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .then(r => r.json())
      .then((res) => {
        if (res.errorMessage) {
          setErrorMsg(res.errorMessage);
        } else {
          messageUser('Sign in was successful, check your email!');
        }
      });
  };

  const isInputValid = (input, validations) => {
    let isValid = true;

    if (validations.required) {
      isValid = isValid && input.length !== 0;
    }
    if (validations.format) {
      isValid = isValid && validations.format.test(input);
    }

    return isValid;
  };

  const classes = () => {
    const isValid = form.email.valid ? 'valid' : 'invalid';
    const isTouched = form.email.touched ? 'touched' : 'untouched';
    return [isValid, isTouched].join('-');
  };

  return (
    <div className='login-page-container'>
      <h1>Sign Up:</h1>

      <p className={errorMsg ? 'errMsg' : ''}>{errorMsg}</p>

      <form className='login-form' onSubmit={handleSubmit}>
        <label htmlFor='email'>Email:</label>
        <input
          type='text'
          key='email'
          name='email'
          className={classes()}
          placeholder={form.email.placeholder}
          value={form.email.value}
          onChange={handleInputChange}
        />
        <br />
        <button className='button' type='submit' disabled={!isFormValid}>Register</button>
      </form>

      <br />
      <Link className='link' to='/login'>Login</Link>
    </div>
  );
};

export default RegisterPage;
