import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { BASE_URL } from '../../common/constants.js';
import decode from 'jwt-decode';
import { login } from '../../auth/actions';
import { Link, useHistory } from 'react-router-dom';
import './RegisterLoginForms.css'
import messageUser from '../../common/commonFunctions.js';

const LoginPage = () => {
  const history = useHistory()
  const dispatch = useDispatch();

  const [errorMsg, setErrorMsg] = useState(null);
  const [isFormValid, setIsFormValid] = useState(false);
  const [form, setForm] = useState({
    name: {
      placeholder: 'Enter name...',
      value: '',
      validations: {
        required: true,
        minLength: 3,
        maxLength: 100
      },
      valid: false,
      touched: false
    },
    password: {
      placeholder: 'Enter password...',
      value: '',
      validations: {
        required: true,
        minLength: 3,
        maxLength: 100
      },
      valid: false,
      touched: false
    }
  });

  const handleInputChange = event => {
    const { name, value } = event.target;

    const updatedElement = { ...form[name] };
    updatedElement.value = value;
    updatedElement.touched = true;
    updatedElement.valid = isInputValid(value, updatedElement.validations);

    const updatedForm = { ...form, [name]: updatedElement }
    setForm(updatedForm);

    const formValid = Object.values(updatedForm).every(elem => elem.valid);
    setIsFormValid(formValid);
  }

  const handleSubmit = event => {
    // do not submit defaultly
    event.preventDefault();

    // build up object for the post request only with necessary data
    const data = Object.keys(form)
      .reduce((acc, elementKey) => {
        return {
          ...acc,
          [elementKey]: form[elementKey].value
        }
      }, {});

    // login
    fetch(`${BASE_URL}/api/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data),
    })
      .then(r => r.json())
      .then((res) => {
        if (res.errorMessage) {
          setErrorMsg(res.errorMessage)
        } else {
          const user = decode(res.token);

          localStorage.setItem('token', res.token);

          dispatch(login(user));

          messageUser('Successfully Logged In.');

          history.push('/user');
        }
      });
  };

  const isInputValid = (input, validations) => {
    let isValid = true;

    if (validations.required) {
      isValid = isValid && input.length !== 0;
    }
    if (validations.minLength) {
      isValid = isValid && input.length >= validations.minLength;
    }
    if (validations.maxLength) {
      isValid = isValid && input.length <= validations.maxLength;
    }

    return isValid;
  };

  const formElements = Object.keys(form)
    .map(name => {
      return {
        id: name,
        config: form[name]
      };
    })
    .map(({ id, config }) => {
      const isValidCSSClass = config.valid ? 'valid' : 'invalid';
      const isTouchedCSSClass = config.touched ? 'touched' : 'untouched';
      const classes = [isValidCSSClass, isTouchedCSSClass].join('-');

      return (
        <div key={id}>
          <br />
          <label htmlFor='id'>{id.charAt(0).toUpperCase() + id.slice(1)}:</label>
          <input
            type={id === 'password' ? 'password' : 'text'}
            key={id}
            name={id}
            className={classes}
            placeholder={config.placeholder}
            value={config.value}
            onChange={handleInputChange}
          />
        </div>
      );
    });

  return (
    <div className='login-page-container'>
      <h1>Sign in:</h1>

      <p className={errorMsg ? 'errMsg' : ''}>{errorMsg}</p>

      <form className='login-form' onSubmit={handleSubmit}>
        {formElements}
        <br />
        <button className='button' type='submit' disabled={!isFormValid}>Login</button>
      </form>

      <br />
      <Link to='/forgot'>Forgot password?</Link>
      <br />
      <Link to='/register'>Register</Link>
    </div>
  );
};

export default LoginPage;
