import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { BASE_URL } from '../../common/constants.js';
import decode from 'jwt-decode';
import { login } from '../../auth/actions';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import './RegisterLoginForms.css'
import messageUser from '../../common/commonFunctions.js';

const ResetPasswordPage = () => {
  const history = useHistory();

  console.log(history);

  let token, user;
  if (!history.location.search) {
    history.push('/home')
  } else {
    token = history.location.search.split('=')[1];
    user = decode(token);

  }

  const dispatch = useDispatch();

  const [errorMsg, setErrorMsg] = useState(null);
  const [isFormValid, setIsFormValid] = useState(false);
  const [form, setForm] = useState({
    password: {
      placeholder: 'Enter password...',
      value: '',
      validations: {
        required: true,
        minLength: 3,
        maxLength: 100
      },
      valid: false,
      touched: false
    },
    confirmPassword: {
      placeholder: 'Enter password again...',
      value: '',
      validations: {
        required: true,
        hasToBeSame: true,
        minLength: 3,
        maxLength: 100
      },
      valid: false,
      touched: false
    }
  });

  const handleInputChange = event => {
    const { name, value } = event.target;

    const updatedElement = { ...form[name] };
    updatedElement.value = value;
    updatedElement.touched = true;
    updatedElement.valid = isInputValid(value, updatedElement.validations);

    const updatedForm = { ...form, [name]: updatedElement }
    setForm(updatedForm);

    const formValid = Object.values(updatedForm).every(elem => elem.valid);
    setIsFormValid(formValid);
  }

  const handleSubmit = async (event) => {
    // do not submit defaultly
    event.preventDefault();

    const password = form.password.value;

    // Reset Password:
    await fetch(`${BASE_URL}/api/users/${user.id}/update`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
      },
      body: JSON.stringify({ password })
    })
      .then(r => r.json())
      .then((res) => {
        if (res.errorMessage) {
          setErrorMsg(res.errorMessage)
        }
      })

    // Login:
    await fetch(`${BASE_URL}/api/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name: user.name,
        password
      }),
    })
      .then(r => r.json())
      .then((res) => {
        if (res.errorMessage) {
          setErrorMsg(res.errorMessage)
        } else {
          const user = decode(res.token);

          localStorage.setItem('token', res.token);

          dispatch(login(user));

          messageUser('Reset Password was Successful!');

          history.push('/user');
        }
      });
  };

  const isInputValid = (input, validations) => {
    let isValid = true;

    if (validations.required) {
      isValid = isValid && input.length !== 0;
    }
    if (validations.hasToBeSame) {
      isValid = isValid && input === form.password.value;
    }
    if (validations.minLength) {
      isValid = isValid && input.length >= validations.minLength;
    }
    if (validations.maxLength) {
      isValid = isValid && input.length <= validations.maxLength;
    }

    return isValid;
  };

  const formElements = Object.keys(form)
    .map(name => {
      return {
        id: name,
        config: form[name]
      };
    })
    .map(({ id, config }) => {
      const isValidCSSClass = config.valid ? 'valid' : 'invalid';
      const isTouchedCSSClass = config.touched ? 'touched' : 'untouched';
      const classes = [isValidCSSClass, isTouchedCSSClass].join('-');

      return (
        <div key={id}>
          <br />
          <label htmlFor='id'>{id === 'password' ? 'New Password:' : 'Confirm Password:'}</label>
          <input
            type='password'
            key={id}
            name={id}
            className={classes}
            placeholder={config.placeholder}
            value={config.value}
            onChange={handleInputChange}
          />
        </div>
      );
    });

  return (
    <div className='login-page-container'>
      <h1>Reset Password:</h1>

      <p className={errorMsg ? 'errMsg' : ''}>{errorMsg}</p>

      <form className='login-form' onSubmit={handleSubmit}>
        {formElements}
        <br />
        <button className='button' type='submit' disabled={!isFormValid}>Reset</button>
      </form>

      <br />
      <Link to='/home'>Back to Home</Link>
    </div>
  );
};

export default ResetPasswordPage;
