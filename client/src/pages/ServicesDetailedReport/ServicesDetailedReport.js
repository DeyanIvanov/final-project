import { BASE_URL } from "../../common/constants";
import useHttp from "../../hooks/use-http";
import { useEffect, useMemo, useState, } from "react";
import DropdownCurrencyMenu from "../../components/Dropdown/DropdownCurrencyMenu";
import CustomerService from "../../components/Services/CustomerService";
import { useParams } from "react-router";


const ServicesDetailedReport = () => {
  const [services, setServices] = useState([]);
  const [currencyValue, setCurrValue] = useState('BGN')
  const [currency, setCurrency] = useState([currencyValue])
  const [rate, setRate] = useState(1);

  const { error, sendRequest: fetchDetailedReport } = useHttp();
  const { errorFetchingCurrencies, sendRequest: fetchCurrencies } = useHttp();
  const { errorFetchingCurrenciesTypes, sendRequest: fetchCurrencyValues } = useHttp();
  const { id } = useParams();

  useEffect(() => {
    fetchCurrencyValues({ url: `https://free.currconv.com/api/v7/currencies?apiKey=a57253e39d32766f7a973` }, (response) => {
      setCurrency(Object.keys(response.results))
    })
  }, [fetchCurrencyValues])


  useEffect(() => {
    fetchDetailedReport({ url: `${BASE_URL}/api/customers/cars/${id}/services/detailed` }, setServices)
  }, [fetchDetailedReport])


  useEffect(() => {
    if (currencyValue === 'BGN') {
      setRate(1);
      return;
    }
    fetchCurrencies({ url: `https://free.currconv.com/api/v7/convert?q=BGN_${currencyValue}&compact=ultra&apiKey=a57253e39d32766f7a97` }, (response) => {
      setRate(response[`BGN_${currencyValue}`]);
    })
  }, [currencyValue], fetchDetailedReport);

  const servicesInSelectedCurrency = useMemo(() => {
    return services.map((ser) => ({
      ...ser,
      priceConverted: (ser.price * rate).toFixed(2),
    }))
  }, [rate, services]);

  return (
    <>
      <div>{services.VIN}</div>
      <div>{services.registration}</div>
      <div>Detailed Report</div>
      {servicesInSelectedCurrency.map(service => <CustomerService {...service} key={services.id} />)}
      <DropdownCurrencyMenu value={currencyValue} currency={currency} handleCurrencyChange={setCurrValue} />
    </>
  )
};

export default ServicesDetailedReport;