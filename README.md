# Snail Garage #

Snail Garage is a fullstack single page application that enables the owners of an auto repair shop to manage their day-to-day job.
This is student project, part of the final assignment of the Telerik Alpha JavaScript track.

<br/>

Frontend Technologies used:


- React - JavaScript library for creating user interfaces

- Redux - A Predictable State Container for JS Apps

<br/>

Backend Technologies used:


- Express - fast node.js network app framework

- mariaDB - Non-blocking MariaDB and MySQL client for Node.js

<br/>

Creators:

Dayana Kirilova - @DaynaKirilova
<br/>
Deyan Ivanov - @DeyanIvanov

# Features: #

- Register new customers, authentication and authorization.
- Forgotten password feature.
- Customers can create vehicles.
- Customers can register shop visit appointments for their cars.
- Getting a PDF invoice of a customer's shop visit/s.
- Employees can see, edit and delete the services that the shop offers.
- Employees are able to see and filter all customers' vehicles.
- Employees can also see and search all shop visits.
- Employees can advance a shop visit's status.
- Employees can ban, unban, update and delete any customer's profile.
- Employees can register a visit for a customer that doesn't have an account yet.

# To Run The Project: #

Install the dependencies and devDependencies for both server and client part

```
$ cd server / client
$ npm install -d
```

- Database

There is a **database.sql** file inside the server folder, open **MySQL Workbench** and copy paste the everything from the **database.sql** file inside a new query. That will create the database.

Fill the database with data run this command inside the server folder

```
$ npm run seed
```

And finally to be able to connect to the database you need to make a **.env** file inside the server folder and type in the settings relevant to your machine. The file should look something like that

```
DB_HOST=localhost
DB_PORT=3306
DB_USER=root
DB_PASS=12345
DB_DATABASE=final_project

PORT=5555

PRIVATE_KEY='some_secret_key'
```