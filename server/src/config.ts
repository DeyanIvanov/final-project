import dotenv from 'dotenv';

dotenv.config();

export const DB_CONFIG: {
  host: string,
  port: number,
  user: string,
  password: string,
  database: string
} = {
  host: process.env.DB_HOST || 'localhost',
  port: Number(process.env.DB_PORT) || 3306,
  user: process.env.DB_USER || 'root',
  password: process.env.DB_PASS || 'root',
  database: process.env.DB_DATABASE || 'final_project',
};

export const PORT: string = process.env.PORT || '5555';

export const PRIVATE_KEY: string = process.env.PRIVATE_KEY || 'SecretKeyBlaBla';

export const TOKEN_LIFETIME: string = process.env.TOKEN_LIFETIME || '1 day';

export const DEFAULT_USER_ROLE: number = 1;

export const EMAIL: string = process.env.EMAIL || 'smart.garage229@gmail.com';

export const EMAIL_PASSWORD: string = process.env.EMAIL_PASSWORD || 'finalprojectpassword123'
