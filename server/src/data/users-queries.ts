import pool from './pool';

const getAll = async () => {
  const sql = `
    SELECT users.id, users.name, users.email, users.phone,
           users.is_banned, roles.role, users.profile_picture
    FROM users JOIN roles ON users.role_id = roles.id
  `;

  return await pool.query(sql);
};

const searchBy = async (column: string, value: string) => {
  const sql = `
    SELECT users.id, users.name, users.email, users.phone,
           users.is_banned, roles.role, users.profile_picture
    FROM users JOIN roles ON users.role_id = roles.id
    AND users.${column} LIKE '%${value}%'
  `;

  return await pool.query(sql, [column, value]);
};

const getBy = async (column: string, value: string) => {
  const sql = `
    SELECT users.id, users.name, users.email, users.phone,
           users.is_banned, roles.role, users.profile_picture
    FROM users JOIN roles ON users.role_id = roles.id
    WHERE users.${column} = ?
  `;

  const result = await pool.query(sql, [value]);

  return result[0];
};

const create = async (email: string, name: string, password: string, roleId: string) => {
  const sql = `
    INSERT INTO users(email, name, password, role_id) 
    VALUES (?, ?, ?, ?)
  `;

  const result = await pool.query(sql, [name, email, password, roleId]);

  return {
    id: result.insertId,
    name,
  };
};

const getWithRole = async (name: string) => {
  const sql = `
    SELECT users.id, users.password, roles.role, users.is_banned
    FROM users JOIN roles ON users.role_id = roles.id 
    WHERE users.name = ?
  `;
  const result = await pool.query(sql, [name]);

  return result[0];
};

const updateUser = async (userId: string, column: string, newValue: string) => {
  const sql = `
    UPDATE users 
    SET ${column} = ?
    WHERE id = ?
  `;

  return await pool.query(sql, [newValue, userId]);
};

const banUser = async (userId: string) => {
  const sqlUpdate = `
      UPDATE users 
      SET is_banned = 1 
      WHERE id = ?
    `;

  return pool.query(sqlUpdate, [userId]);
};

const unBanUser = async (userId: string) => {
  const sqlUpdate = `
      UPDATE users 
      SET is_banned = 0 
      WHERE id = ?
    `;

  return pool.query(sqlUpdate, [userId]);
};

const removeUser = async (userId: string) => {
  const sql = `
    DELETE FROM users
    WHERE id = ?
  `;

  return await pool.query(sql, [userId]);
};

const logoutUser = async (token: string, userId: string) => {
  const sql = `
    INSERT INTO tokens(token, user_id)
    VALUES (?, ?)
  `;

  return await pool.query(sql, [token, userId]);
};

const uploadImage = async (img: string, userId: string) => {
  const sql = `
    UPDATE users
    SET profile_picture = ?
    WHERE id = ?
  `;

  return pool.query(sql, [`avatars/${img}`, userId]);
}

export default {
  getAll,
  searchBy,
  getBy,
  create,
  getWithRole,
  updateUser,
  banUser,
  unBanUser,
  removeUser,
  logoutUser,
  uploadImage,
};
