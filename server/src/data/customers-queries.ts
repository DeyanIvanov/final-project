import pool from './pool';

const getCustomerById = async (id: string) => {
  const sql = `
    SELECT users.id, users.name, users.email, users.phone,
           users.is_banned, roles.role, users.profile_picture
    FROM users JOIN roles ON users.role_id = roles.id
    WHERE users.id = ?
  `;

  const result = await pool.query(sql, [id]);

  return result[0];
};

const getVehiclesByCustomer = async (customerId: number) => {
  const sql = `
    SELECT  users.id AS userId, users.name, vehicles.id, cars.brand, cars.model, cars.years,
            vehicles.registration, vehicles.VIN
    FROM users
      JOIN vehicles ON vehicles.owner_id=users.id
      JOIN cars ON cars.id = vehicles.car_id
    WHERE users.id = ?
  `;

  return await pool.query(sql, [customerId]);
};

const getShopVisits = async () => {
  const sql = `
    SELECT history.id, users.id AS customerId, users.name AS customer, users.email, services.service,
           history.price, history.status, history.start_date, history.end_date,
           cars.brand, cars.model, cars.years,
           vehicles.VIN, vehicles.registration
    FROM users 
      JOIN history ON users.id = history.customer_id
      JOIN services ON history.service_id = services.id
      JOIN vehicles ON history.vehicle_id = vehicles.id
      JOIN cars ON vehicles.car_id = cars.id
    ORDER BY history.id DESC;
  `;

  return await pool.query(sql);
};

const getShopVisitsByUser = async (id: string) => {
  const sql = `
    SELECT history.id, users.id AS customerId, users.name AS customer, users.email, services.service,
         history.price, history.status, history.start_date, history.end_date,
         cars.brand, cars.model, cars.years,
         vehicles.VIN, vehicles.registration
    FROM users 
      JOIN history ON users.id = history.customer_id
      JOIN services ON history.service_id = services.id
      JOIN vehicles ON history.vehicle_id = vehicles.id
      JOIN cars ON vehicles.car_id = cars.id
    WHERE users.id = ?
    ORDER BY history.id DESC;
  `;

  return await pool.query(sql, [id]);
};

const createVisit = async (customerId: string, vehicleId: string, serviceId: string, price: string) => {
  const sql = `
    INSERT INTO history(service_id, customer_id, vehicle_id, price, status) 
    VALUES (?, ?, ?, ?, 'Not Started')
  `;

  return await pool.query(sql, [serviceId, customerId, vehicleId, price]);
};

const updateStatusStart = async (visitId: string) => {
  const date = new Date().toLocaleDateString('fr-CA');

  const sql = `
    UPDATE history 
    SET status = 'In Progress', start_date = '${date}'
    WHERE id = ?
    `;

  return await pool.query(sql, [visitId]);
}

const updateStatusFinish = async (visitId: string) => {
  const date = new Date().toLocaleDateString('fr-CA');

  const sql = `
    UPDATE history 
    SET status = 'Ready For Pickup', end_date = '${date}'
    WHERE id = ?
    `;

  return await pool.query(sql, [visitId]);
}

const getServices = async () => {
  const sql = `
    SELECT  id, service, price
    FROM services
  `;

  return await pool.query(sql);
};
const getAllCarsByCustomer = async (customerId: number) => {
  const sql = `
  SELECT vehicles.id, cars.model, cars.brand from cars
  left join vehicles on vehicles.car_id=cars.id
  left join users on vehicles.owner_id=users.id
  where users.id =?`


  return await pool.query(sql, [customerId]);
};


const getAllServicesByCarAndDate = async (valueNumber: number, sort: string) => {
  const sql = `
  SELECT history.id, history.start_date as date, prices.price, services.service, history.cars_id as carId
  From final_project.history
  left join final_project.prices on history.prices_id= prices.id
  left join final_project.services on prices.services_id=services.id
  where history.cars_id= ?
  order by history.start_date ${sort}
  `;
  console.log(sort, 'sd')
  return await pool.query(sql, [valueNumber, sort]);
};

const getAllServicesByCar = async (vehicleId: number, sort = 'asc', from: string, to: string) => {
  const sql = `
    SELECT history.price, history.status, history.start_date as date, services.service, users.name,vehicles.id FROM final_project.history
    left join services on history.service_id=services.id
    left join vehicles on history.vehicle_id= vehicles.id
    left join users on vehicles.owner_id = users.id
    where vehicles.id = ?
    ${from ? `and history.start_date > '${from}'` : ''}
    ${to ? `and history.start_date < '${from}'` : ''}
    ${sort ? `order by history.start_date ${sort}` : ''} 
  `;
  console.log(sql, 'sql')

  return await pool.query(sql, [vehicleId]);
};

const getFilteredAndSortedServicesByDate = async (vehicleId: number, sort = 'asc', from: string, to: string) => {
  if (!(from && to)) {
    const sql = `
    SELECT history.price, history.status, history.start_date as date, services.service, users.name, vehicles.id FROM final_project.history
    left join services on history.service_id=services.id
    left join vehicles on history.vehicle_id = vehicles.id
    left join users on vehicles.owner_id = users.id 
      where vehicles.id = ?
  order by history.start_date ${sort}
  `;
    return await pool.query(sql, [vehicleId])
  } else {
    const sql = `
    SELECT history.price, history.status, history.start_date as date, services.service, users.name,vehicles.id FROM final_project.history
    left join services on history.service_id=services.id
    left join vehicles on history.vehicle_id= vehicles.id
    left join users on vehicles.owner_id = users.id
    where vehicles.id = ?
    and history.start_date Between '${from}' And '${to}'
    order by history.start_date ${sort}
     `
    return await pool.query(sql, [vehicleId])
  }

};

const getServicesDetailedReport = async (valueNumber: number) => {
  const sql = ` SELECT history.price, history.status, history.start_date as date, services.service, users.name,vehicles.id FROM final_project.history
left join services on history.service_id=services.id
left join users on history.customer_id= users.id
left join vehicles on history.vehicle_id= vehicles.id
  where vehicles.id =?`


  return await pool.query(sql, [valueNumber])
};

export default {
  getCustomerById,
  getVehiclesByCustomer,
  getShopVisits,
  getShopVisitsByUser,
  createVisit,
  getServices,
  updateStatusStart,
  updateStatusFinish,
  getAllCarsByCustomer,

  getAllServicesByCarAndDate,
  getAllServicesByCar,
  getFilteredAndSortedServicesByDate,
  getServicesDetailedReport,
}
