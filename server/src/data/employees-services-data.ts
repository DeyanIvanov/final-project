import pool from './pool';

const getAllServices = async () => {
  const sql = `
    SELECT services.id ,services.service, services.price
    FROM services
    where is_deleted =0
  `;

  return await pool.query(sql);
};

const create = async (price: number, service: string) => {

  const sql = `
    INSERT INTO services(service, price) 
    VALUES (?, ?)
  `;

  const result = await pool.query(sql, [ service, price]);

  return {
    id: result.insertId,
    price,
    service,
  };
};

const getServiceById = async (serviceId: number) => {
  const sql = `
    SELECT id, service, price 
    FROM services
    WHERE id = ?
  `;
const result = await pool.query(sql, [serviceId]);
return result && result.length > 0 ? result[0] : null;
};

//to fix:any
const update = async (service: any) => {
  const {
    id, price, serviceName
  } = service;

  const sql = `
    UPDATE services
    SET service = ?,
        price = ?
    WHERE id = ?
  `;

  return await pool.query(sql, [service.service, price, id]);
};

//to fix()
const remove = async (value: any) => {
  const sql = `
    UPDATE services 
    SET is_deleted = 1
    WHERE id = ?
  `;

  return await pool.query(sql, [value.id]);
};

const getServiceByName = async (value: string) => {
  const sql = `
    SELECT id, service, price
    FROM services
    ${value ? `WHERE service LIKE '%${value}%'` : ''}
  `;

  return await pool.query(sql);
};

const getByPrice = async (minValue: number, maxValue: number) => {
  const sql = `
    SELECT id,price,service
    FROM services
    WHERE price BETWEEN ? AND ?
  `;

  return await pool.query(sql, [minValue, maxValue]);
};


export const employeesServicesData: any = {
  getAllServices,
  getServiceById,
  create,
  update,
  remove,
  getServiceByName,
  getByPrice
};
