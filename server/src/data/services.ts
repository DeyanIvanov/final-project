const services = [
  {
    service: 'Oil/oil filter changed',
    price: 100,
  },
  {
    service: 'Replace air filter',
    price: 18.50,
  },
  {
    service: 'Scheduled maintenance',
    price: 27.50,
  },
  {
    service: 'New tires',
    price: 50.80,
  },
  {
    service: 'Battery replacement',
    price: 170,
  },
  {
    service: 'Brake work',
    price: 30,
  },
  {
    service: 'Antifreeze added',
    price: 19.99,
  },
  {
    service: 'Engine tune-up',
    price: 180,
  },
  {
    service: 'Suspension change',
    price: 400,
  },
  {
    service: 'Wheels alignment',
    price: 25.50,
  },
  {
    service: 'Wiper blades replacement',
    price: 10.50,
  },
]

export default services;