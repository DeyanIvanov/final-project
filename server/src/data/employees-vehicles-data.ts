import pool from './pool';

type VehicleColumn = 'id' | 'cars_id' | 'registration' | 'VIN';

export interface Vehicle {
  name: string;
  brand: string;
  model: string;
  cars_id: number;
  registration: string;
  VIN: string;
  id: number;
}

const getCars = async () => {
  const sql = `
    SELECT *
    FROM cars
  `;

  return await pool.query(sql);
}

const getBy = (column: VehicleColumn) => async (columnValue: number | string | boolean): Promise<Vehicle | null> => {
  const sql = `
    SELECT users.id, users.name, vehicles.id AS vehicleId,
           cars.brand, cars.model, cars.years, registration, VIN
    FROM vehicles
      JOIN cars  on vehicles.car_id=cars.id
      JOIN users on vehicles.owner_id=users.id 
    WHERE vehicles.${column} = ?
  `;

  const vehicles = await pool.query(sql, [columnValue]);

  return vehicles && vehicles.length > 0 ? vehicles[0] : null;
};

const getById = getBy('id');

const getAllVehicles = async () => {
  const sql = `
    SELECT users.id, users.name, vehicles.id AS vehicleId,
           cars.brand, cars.model, cars.years, vehicles.registration, vehicles.VIN
    FROM vehicles
      JOIN cars  ON vehicles.car_id=cars.id
      JOIN users ON vehicles.owner_id=users.id 
  `;

  return await pool.query(sql);
};

const getSingleVehicle = async (valueNumber: number) => {

  const sql = `
    SELECT users.id, users.name, vehicles.car_id,
           vehicles.id AS vehicleId, cars.brand,
           cars.model, vehicles.registration, vehicles.VIN
    FROM vehicles
      JOIN cars  ON vehicles.car_id=cars.id
      JOIN users ON vehicles.owner_id=users.id 
    WHERE vehicles.id = ?
  `;

  const vehicles = await pool.query(sql, [valueNumber]);

  return vehicles && vehicles.length > 0 ? vehicles[0] : null;
};

//to fix:any
const update = async (vehicle: any) => {
  const {
    id, registration, VIN, cars_id, users_id
  } = vehicle;

  const sql = `
    UPDATE vehicles 
    SET registration = ?, 
        VIN = ?,
        car_id =?,
        owner_id = ?
    WHERE id = ?
  `;

  return await pool.query(sql, [registration, VIN, cars_id, users_id, id]);
};

const getAllBrands = async () => {
  const sql = `
    SELECT DISTINCT cars.brand
    FROM cars
  `;

  return await pool.query(sql)
};

const getAllModelsAndYears = async (modelName: string) => {
  const sql = `
    SELECT cars.id, cars.model, cars.years
    FROM cars
    WHERE cars.brand = ?`
  return await pool.query(sql, [modelName])
};

const createVehicle = async (registration: string, VIN: string, cars_id: number, users_id: number) => {

  const sql = `
    INSERT INTO vehicles(registration, VIN, car_id, owner_id) 
    VALUES (?, ?, ?, ?)
  `;
  const result = await pool.query(sql, [registration, VIN, cars_id, users_id]);

  return {
    id: result.insertId,
    registration,
    VIN,
    cars_id,
    users_id,
  };
};

const getVehiclesByUser = async (value: string) => {
  const sql = `
    SELECT users.id, users.name, vehicles.id AS vehicleId,
      cars.brand, cars.model, cars.years, vehicles.registration, vehicles.VIN
    FROM vehicles
    JOIN cars  ON vehicles.car_id=cars.id
    JOIN users ON vehicles.owner_id=users.id 
    WHERE users.name LIKE '%${value}%'
  `;

  return await pool.query(sql);
};

const getUserIdByName = async (name: string) => {
  const sql = `
    SELECT id
    FROM users
    WHERE name = ?
  `;

  const users = await pool.query(sql, [name]);

  return users.length > 0 ? users[0].id : null;
};


export const employeesVehiclesData: any = {
  getCars,
  getById,
  getAllVehicles,
  getSingleVehicle,
  update,
  getAllBrands,
  getAllModelsAndYears,
  createVehicle,
  getVehiclesByUser,
  getUserIdByName,

}
