const cars = [
  {
    brand:'Alfa Romeo',
    model:'156',
    years: '1997–2007'
  },
  {
    brand:'Alfa Romeo',
    model:'159',
    years: '2005–2011'
  },
  {
    brand:'Alfa Romeo',
    model:'gt',
    years: '2003–2010'
  },
  // Audi --------------------
  {
    brand:'Audi',
    model:'A3',
    years: '1996-2020'
  },
  {
    brand:'Audi',
    model:'A4',
    years: '1994–2019'
  },
  {
    brand:'Audi',
    model:'A8',
    years: '2010–2018'
  },
  {
    brand:'Audi',
    model:'TT',
    years: '1998–2019'
  },
  // BMW ---------------
  {
    brand:'BMW',
    model:'3 series',
    years: '1975–2021'
  },
  {
    brand:'BMW',
    model:'5 series',
    years: '2016–2021'
  },
  // Chevrolet  --------------------
  {
    brand:'Chevrolet',
    model:'Aveo',
    years: '2002–2020'
  },
  {
    brand:'Chevrolet',
    model:'Spark',
    years: '1998–2020'
  },
  {
    brand:'Chevrolet',
    model:'Nubira',
    years: '1997–2020'
  },
  // Citroën ------------------------
  {
    brand:'Citroën',
    model:'C3',
    years: '2002–2020'
  },
  {
    brand:'Citroën',
    model:'C4',
    years: '1998–2020'
  },
  {
    brand:'Citroën',
    model:'C5',
    years: '1997–2020'
  },
  // Dacia ---------------------------
  {
    brand:'Dacia',
    model:'Duster',
    years: '2002–2020'
  },
  {
    brand:'Dacia',
    model:'Dokker',
    years: '1998–2020'
  },
  {
    brand:'Dacia',
    model:'Logan',
    years: '1997–2020'
  },
  // Daihatsu ---------------------------
  {
    brand:'Daihatsu',
    model:'Sirion',
    years: '2002–2020'
  },
  {
    brand:'Daihatsu',
    model:'Terios',
    years: '1998–2020'
  },
  
  // Ford ----------------------------
  {
    brand:'Ford',
    model:'Fiesta',
    years: '1976–2021'
  },
  {
    brand:'Ford',
    model:'Focus',
    years: '1998–2021'
  },
  {
    brand:'Ford',
    model:'Mustang',
    years: '1965–2021'
  },
  // Fiat ----------------------------
  {
    brand:'Fiat',
    model:'Panda',
    years: '1980–2021'
  },
  {
    brand:'Fiat',
    model:'Punto',
    years: '1980–2021'
  },
  // Great Wall ----------------------
  {
    brand:'Great Wall',
    model:'Hover',
    years: '1998–2020'
  },
  {
    brand:'Great Wall',
    model:'Steed',
    years: '1998–2020'
  },
  // Honda ----------------------------
  {
    brand:'Honda',
    model:'Civic',
    years: '1972–2021'
  },
  {
    brand:'Honda',
    model:'Accord',
    years: '1976–2021'
  },
  {
    brand:'Honda',
    model:'CR-V',
    years: '1995–2021'
  },
  // Hyundai ---------------------------
  {
    brand:'Hyundai',
    model:'Coupe',
    years: '1972–2021'
  },
  {
    brand:'Hyundai',
    model:'i10',
    years: '1976–2021'
  },
  // Jeep -----------------------------
  {
    brand:'Jeep',
    model:'Cherokee',
    years: '1972–2021'
  },
  {
    brand:'Jeep',
    model:'Patriot',
    years: '1976–2021'
  },
  // Kia -------------------------------
  {
    brand:'Kia',
    model:'Carens',
    years: '1972–2021'
  },
  {
    brand:'Kia',
    model:'Sorento',
    years: '1976–2021'
  },
  // Lada ------------------------
  {
    brand:'Lada',
    model:'Niva',
    years: '1976–2021'
  },
  // Land Rover ------------------------
  {
    brand:'Land Rover',
    model:'Range Rover',
    years: '1976–2021'
  },
  {
    brand:'Land Rover',
    model:'Freelander',
    years: '1976–2021'
  },
  {
    brand:'Land Rover',
    model:'Discovery',
    years: '1976–2021'
  },
  // Lexus ------------------------
  {
    brand:'Lexus',
    model:'IS',
    years: '1976–2021'
  },
  {
    brand:'Lexus',
    model:'GS',
    years: '1976–2021'
  },
  // Mazda ------------------------
  {
    brand:'Mazda',
    model:'2',
    years: '1976–2021'
  },
  {
    brand:'Mazda',
    model:'3',
    years: '1976–2021'
  },
  {
    brand:'Mazda',
    model:'6',
    years: '1976–2021'
  },
  // Mercedes ------------------------
  {
    brand:'Mercedes',
    model:'A class',
    years: '1976–2021'
  },
  {
    brand:'Mercedes',
    model:'C class',
    years: '1976–2021'
  },
  {
    brand:'Mercedes',
    model:'E class',
    years: '1976–2021'
  },
  // Mini ------------------------
  {
    brand:'Mini',
    model:'Cooper',
    years: '1976–2021'
  },
  // Mitsubishi ------------------------
  {
    brand:'Mitsubishi',
    model:'Pajero',
    years: '1976–2021'
  },
  {
    brand:'Mitsubishi',
    model:'Lancer',
    years: '1976–2021'
  },
  // Nissan ------------------------
  {
    brand:'Nissan',
    model:'Micra',
    years: '1976–2021'
  },
  {
    brand:'Nissan',
    model:'Juke',
    years: '1976–2021'
  },
  // Opel ------------------------
  {
    brand:'Opel',
    model:'Astra',
    years: '1976–2021'
  },
  {
    brand:'Opel',
    model:'Corsa',
    years: '1976–2021'
  },
  // Peugeot ------------------------
  {
    brand:'Peugeot',
    model:'308',
    years: '1976–2021'
  },
  {
    brand:'Peugeot',
    model:'508',
    years: '1976–2021'
  },
  // Renault ------------------------
  {
    brand:'Renault',
    model:'Megan',
    years: '1976–2021'
  },
  {
    brand:'Renault',
    model:'Clio',
    years: '1976–2021'
  },
  // Seat ------------------------
  {
    brand:'Seat',
    model:'Alhambra',
    years: '1976–2021'
  },
  {
    brand:'Seat',
    model:'Leon',
    years: '1976–2021'
  },
  // Skoda ------------------------
  {
    brand:'Skoda',
    model:'Octavia',
    years: '1976–2021'
  },
  // Subaru ------------------------
  {
    brand:'Subaru',
    model:'Legacy',
    years: '1976–2021'
  },
  {
    brand:'Subaru',
    model:'Impreza',
    years: '1976–2021'
  },
  // Toyota ------------------------
  {
    brand:'Toyota',
    model:'Corola',
    years: '1976–2021'
  },
  {
    brand:'Toyota',
    model:'Celica',
    years: '1976–2021'
  },
];

export default cars;
