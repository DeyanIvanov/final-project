import pool from './pool';

const tokenExists = async (token: string) => {
  const sql = `
    SELECT * 
    FROM tokens 
    WHERE tokens.token LIKE '%${token}%'
  `;

  const result = await pool.query(sql, [token]);

  return result && result.length > 0;
};

export default tokenExists;
