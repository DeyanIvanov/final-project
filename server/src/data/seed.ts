import bcrypt from 'bcrypt';
import db from './pool';
import cars from './cars';
import services from './services';

(async () => {
  // insert roles
  const result3 = await db.query('SELECT * FROM roles');
  if (result3.length === 0) {
    console.log('Creating roles...');

    await db.query(`
      INSERT INTO roles (role)
      VALUES (?)
    `, ['customer']);

    await db.query(`
      INSERT INTO roles (role)
      VALUES (?)
    `, ['employee']);

    console.log('Done.');
  }

  // insert employee
  const result1 = await db.query('SELECT * FROM final_project.users WHERE role_id = ?', [2]);

  if (!result1 || result1.length === 0) {
    console.log('Creating employee...');

    const result2 = await db.query(`
      INSERT INTO final_project.users (email, name, password, role_id)
      VALUES (?, ?, ?, ?)
    `, ['smart.garage229@gmail.com', 'Employee', await bcrypt.hash('employee', 10), 2]);

    console.log(result2);
  }

  await Promise.all(cars.map(({
    brand, model, years
  }) => db.query(`INSERT INTO final_project.cars (brand, model, years)
  VALUES (?, ?, ?)`, [brand, model, years])));

  await Promise.all(services.map(({
    service, price
  }) => db.query(`INSERT INTO services (service, price)
  VALUES (?, ?)`, [service, price])));

  db.end();
})()
  .catch(console.log);
