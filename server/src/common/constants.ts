export const usersConstants = {
  nameMinLength: 0,
  nameMaxLength: 100,
  emailRegex: /^[\w.]+@([\w-]+\.)+[\w-]{2,4}$/,
  passwordMinLength: 3,
  passwordMaxLength: 100,
  phoneRegex: /((\(\+359\)|0)8[789]\d{1}(|-| )\d{3}(|-| )\d{3}|08[7-9](|-| )\d{3}(|-| )\d{4})/
};

export const vehiclesConstants = {
  VIN:/^(?<wmi>[A-HJ-NPR-Z\d]{3})(?<vds>[A-HJ-NPR-Z\d]{5})(?<check>[\dX])(?<vis>(?<year>[A-HJ-NPR-Z\d])(?<plant>[A-HJ-NPR-Z\d])(?<seq>[A-HJ-NPR-Z\d]{6}))$/,
  registration: /^[a-z]{1,2}[0-9]{4}[a-z]{1,2}$/i
  // vin: (value) => typeof value === 'string' &&
  // /^(?<wmi>[A-HJ-NPR-Z\d]{3})(?<vds>[A-HJ-NPR-Z\d]{5})(?<check>[\dX])(?<vis>(?<year>[A-HJ-NPR-Z\d])(?<plant>[A-HJ-NPR-Z\d])(?<seq>[A-HJ-NPR-Z\d]{6}))$/.test(value),
  //register(value): /^[a-z]{1,2}[0-9]{4}[a-z]{1,2}$/i.test(value),
}

export const servicesConstants ={
  serviceMinLength: 3,
}