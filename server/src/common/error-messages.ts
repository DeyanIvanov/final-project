import { usersConstants } from './constants';

export default {
  users: {
    email: 'Expected email in valid format (smth@smth.smth)',
    name: `Expected string with length [${usersConstants.nameMinLength}-${usersConstants.nameMaxLength}]`,
    password: `Expected string with length [${usersConstants.passwordMinLength}-${usersConstants.passwordMaxLength}]`,
    role: ' Expected either - "customer" or "employee".',
  },
};
