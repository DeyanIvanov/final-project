import express, { Application, Request, Response } from 'express';
import helmet from 'helmet';
import cors from 'cors';
import passport from 'passport';
import jwtStrategy from './auth/strategy';
import employeeVehiclesRouter from './routes/employees-vehicles-routes'
import usersRouter from './routes/user-routes';
import employeeServicesRouter from './routes/employees-services-routes'
import customerRouter from './routes/customer-routes';
// import employeeRouter from './routes/employee-routes';


import { PORT } from './config'

const app: Application = express();

app.use(cors());
app.use(helmet());
app.use(express.json());

passport.use(jwtStrategy);
app.use(passport.initialize());

app.use('/api/users/', usersRouter);
app.use('/api/employee/vehicles', employeeVehiclesRouter)
app.use('/api/employee/services', employeeServicesRouter)
app.use('/api/customers/', customerRouter)
// app.use('api/employee' , employeeRouter)

app.use('/avatars', express.static('avatars'));

app.all('*', (req: Request, res: Response) => res.status(404).send({ message: 'Resource not found!' }));

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
