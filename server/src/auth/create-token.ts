import jwt from 'jsonwebtoken';
import { PRIVATE_KEY } from '../config'

const createToken = (payload: object, expiresIn: object) => {
  const token = jwt.sign(
    payload,
    PRIVATE_KEY,
    expiresIn,
  );

  return token;
};

export default createToken;
