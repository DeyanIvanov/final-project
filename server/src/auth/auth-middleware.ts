import { Request, Response, NextFunction } from 'express'
import passport from 'passport';
import tokenExists from '../data/token-data'

const authenticateToken = passport.authenticate('jwt', { session: false });

/** 
 * Middleware that checks if there is logged user and if there is, it adds his info in 'req.user'. 
 */
export const authenticationMiddleware = async (req: Request, res: Response, next: NextFunction) => {
  // logic to check if that token is not an invalidated one (logged out)
  let token = req.headers.authorization;
  if (!token) {
    return res.status(401).send({ error: 'No token provided!' });
  }
  token = token.replace('Bearer ', '');

  const isTokenInvalidated = await tokenExists(token);

  if (isTokenInvalidated) {
    return res.status(401).json({ error: 'Your token is no longer valid.' });
  }

  return authenticateToken(req, res, next);
};

/** 
 * Middleware that checks if the logged user has a specific role. 
 */
export const roleMiddleware = (roleName: 'customer' | 'employee') => {
  return (req: any, res: Response, next: NextFunction) => {
    if (req.user && req.user.role === roleName) {
      next();
    } else {
      res
        .status(403)
        .send({ message: 'Resource is forbidden!' });
    }
  };
};

/**
 * Middleware that checks if logged user is the same as the one we're getting the data for, or is employee
 */
export const identityMiddleware = (req: Request, res: Response, next: NextFunction) => {
  const id = Number(req.params.id);
  const userId = Number((req?.user as any)?.id);
  const userRole = (req?.user as any)?.role;

  if (+id === +userId || userRole === 'employee') {
    next();
  } else {
    res
      .status(403)
      .send({ message: 'Forbidden to owner of resource and employees only!' });
  }
};
