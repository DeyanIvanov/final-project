import { Request, Response, NextFunction } from 'express'

const createValidator = (schema: any) => {
  return (req: Request, res: Response, next: NextFunction) => {
    const { body } = req;
    const validations = Object.keys(schema);

    const fails = validations
      .map(v => schema[v](body[v]))
      .filter(e => e != null);

    if (fails.length > 0) {
      const response: { message: string[] } = { message: fails, };

      res.status(400).send(response);
    } else {
      next();
    }
  };
};

export default createValidator;
