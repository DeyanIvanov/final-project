export default {
  vehicleId: (value: string) => {
    if (!value) {
      return 'Input vehicleId please.';
    }

    if (typeof +value !== 'number') {
      return 'vehicleId should be string of a number.';
    }

    return null;
  },
  serviceId: (value: string) => {
    if (!value) {
      return 'Input serviceId please.';
    }

    if (typeof +value !== 'number') {
      return 'serviceId should be string of a number.';
    }

    return null;
  },
  price: (value: string) => {
    if (!value) {
      return 'Input price please.';
    }

    if (typeof +value !== 'number') {
      return 'price should be string of a number.';
    }

    return null;
  },
};
