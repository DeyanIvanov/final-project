import { servicesConstants } from '../../common/constants'

export default {

  registration: (value: string) => {
    if (!value) {
      return null;
    }
    if (value !== "") {
      return `The service must be a string`;
    }

    if (value.length < (servicesConstants.serviceMinLength)) {
      return `The service must be at least ${servicesConstants.serviceMinLength} symbols long`;
    }

    return null;
  },
  price: (value: number) => {
    if (!value) {
      return null;
    }
    return null;
  },

};