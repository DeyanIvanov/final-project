import { usersConstants } from '../../common/constants';

export default {
  email: (value: string) => {
    if (!value) {
      return 'Input Email please.';
    }

    if (typeof value !== 'string') {
      return 'Email should be string.';
    }

    if (!usersConstants.emailRegex.test(value)) {
      return 'Email is not in valid format.';
    }

    return null;
  }
};
