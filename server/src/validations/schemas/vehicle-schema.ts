import { usersConstants, vehiclesConstants } from '../../common/constants'

export default (requiredFields: boolean) => ({
  registration: (value: string) => {
    if (!value) {
      return requiredFields ? `Registration is required!` : null;
    }

    if (!value.match(vehiclesConstants.registration)) {
      return `The Registration must be string with the correct symbols`;
    }

    return null;
  },
  VIN: (value: string) => {
    if (!value) {
      return requiredFields ? 'VIN is required!' : null
    }

    if (!value.match(vehiclesConstants.VIN)) {
      return `The VIN must be string with the correct symbols`;
    }

    return null;
  },
  username: (value: string) => {
    if (!value) {
      return 'Username field is required';
    }

    if (value.length < usersConstants.nameMinLength) {
      return `Username must be a string with at least ${usersConstants.nameMinLength} symbols`;
    }

    if (value.length > usersConstants.nameMaxLength) {
      return `Username must not exceed ${usersConstants.nameMaxLength} symbols`;
    }

    return null;
  },
  cars_id: (value: number | string) => {
    if (!value) {
      return requiredFields ? 'Cars_id is required' : null;
    }

    if (Number.isNaN(Number(value))) {
      return 'Cars_id must be a positive number';
    }

    return null;
  }
});