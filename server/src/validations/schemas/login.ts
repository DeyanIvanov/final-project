import { usersConstants } from '../../common/constants';

export default {
  name: (value: string) => {
    if (!value) {
      return 'Input Name please.';
    }

    if (typeof value !== 'string'
      || value.length > usersConstants.nameMaxLength
      || value.length < usersConstants.nameMinLength) {
      return `Username's length should be from ${usersConstants.nameMinLength} to ${usersConstants.nameMaxLength} characters long.`;
    }

    return null;
  },
  password: (value: string) => {
    if (!value) {
      return 'Input Password please.';
    }

    if (typeof value !== 'string'
      || value.length < usersConstants.passwordMinLength
      || value.length > usersConstants.passwordMaxLength) {
      return `Password's length should be from ${usersConstants.passwordMinLength} to ${usersConstants.passwordMaxLength} characters long.`;
    }

    return null;
  },
};
