import serviceErrors from './service-errors';
import generator from 'generate-password';
import bcrypt from 'bcrypt';
import { DEFAULT_USER_ROLE } from '../config';
import sendSignUpEmail from '../common/send-signup-email';
import sendResetPasswordEmail from '../common/send-reset-password-emil';
import createToken from '../auth/create-token';

const getAllUsers = (usersData: usersDataFunctions) => {
  return async (search: string) => {
    return search
      ? await usersData.searchBy('name', search)
      : await usersData.getAll();
  };
};

const getUserById = (usersData: usersDataFunctions) => {
  return async (id: string) => {
    const user = await usersData.getBy('id', id);

    if (!user) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }
    return { error: null, user };
  };
};

const updateUser = (usersData: usersDataFunctions) => {
  return async (id: string, column: string, newValue: string) => {
    const user = await usersData.getBy('id', id);

    if (!user) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }
    await usersData.updateUser(id, column, newValue);
    return { error: null, user };
  };
};

// Register
const createUser = (usersData: usersDataFunctions) => {
  return async (email: string) => {
    const existingUser = await usersData.getBy('email', email);

    if (existingUser) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        user: null,
      };
    }

    const name = email.split('@')[0];

    const password = generator.generate({
      length: 10,
      numbers: true
    });

    sendSignUpEmail(email, name, password)

    const passwordHash = await bcrypt.hash(password, 10);
    const user = await usersData.create(name, email, passwordHash, DEFAULT_USER_ROLE);

    return { error: null, user };
  };
};

const signInUser = (usersData: usersDataFunctions) => {
  return async (name: string, password: string) => {
    const user = await usersData.getWithRole(name);
console.log(user)
    if (!user || !(await bcrypt.compare(password, user.password))) {
      return {
        error: serviceErrors.INVALID_SIGNIN,
        user: null,
      };
    }

    if (user.isBanned === 1) {
      return {
        error: serviceErrors.BANNED_USER,
        user: null,
      };
    }

    return {
      error: null,
      user,
    };
  };
};

const logOutUser = (usersData: usersDataFunctions) => {
  return async (token: string, userId: string) => {
    const userToLogout = await usersData.logoutUser(token, userId);

    return { error: null, user: userToLogout };
  };
};

const banUser = (usersData: usersDataFunctions) => {
  return async (id: number) => {
    const updateResult = await usersData.banUser(id);
    if (updateResult.affectedRows > 0) {
      return { error: null };
    }

    return {
      error: serviceErrors.RECORD_NOT_FOUND,
    };
  };
};

const unBanUser = (usersData: usersDataFunctions) => {
  return async (id: number) => {
    const updateResult = await usersData.unBanUser(id);
    if (updateResult.affectedRows > 0) {
      return { error: null };
    }

    return {
      error: serviceErrors.RECORD_NOT_FOUND,
    };
  };
};

const deleteUser = (usersData: usersDataFunctions) => {
  return async (id: number) => {
    const userToDelete = await usersData.getBy('id', id);

    if (!userToDelete) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    await usersData.removeUser(id);

    return { error: null, user: userToDelete };
  };
};

const forgottenPass = (usersData: usersDataFunctions) => {
  return async (email: string) => {
    const existingUser = await usersData.getBy('email', email);

    if (!existingUser) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    const payload = {
      id: existingUser.id,
      name: existingUser.name,
      role: existingUser.role,
    };

    const token = createToken(payload, { expiresIn: '1h' });

    // Sends email with link to resetPassword form
    sendResetPasswordEmail(email, token);

    return { error: null, user: existingUser }
  };
};

const uploadAvatar = (usersData: usersDataFunctions) => {
  return async (filePath: string, id: string) => {
    const user = await usersData.getBy('id', id);

    if (!user) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    await usersData.uploadImage(filePath, id);

    return { error: null, user: user };
  };
};

export default {
  getAllUsers,
  getUserById,
  updateUser,
  createUser,
  signInUser,
  logOutUser,
  banUser,
  unBanUser,
  deleteUser,
  forgottenPass,
  uploadAvatar,
}

interface usersDataFunctions {
  searchBy: Function;
  getAll: Function;
  getBy: Function;
  updateUser: Function;
  create: Function;
  getWithRole: Function;
  logoutUser: Function;
  banUser: Function;
  unBanUser: Function;
  removeUser: Function;
  uploadImage: Function;
}
