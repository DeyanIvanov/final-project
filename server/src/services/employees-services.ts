import { Vehicle } from '../data/employees-vehicles-data';
import services from '../data/services';
import serviceErrors from './service-errors';

const getAllCars = (employeesData: { getCars: any; }) => {
  return async () => {
    const cars = await employeesData.getCars();

    return cars;
  };
};

const getVehicles = (employeesData: { getAllVehicles: any; }) => {
  return async () => {

    const vehicles = await employeesData.getAllVehicles();

    if (!vehicles) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        vehicles: null,
      };
    }
    return { error: null, vehicles };
  }
};

const getVehicleReport = (employeesVehiclesData: { getById: (id: number) => Vehicle; }) => {
  return async (vehicleId: number) => {
    const vehicle = await employeesVehiclesData.getById(vehicleId);

    if (!vehicle) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        cars: null,
      };
    }
    return { error: null, vehicle };
  };
};

const updateVehicle = (employeesVehiclesData: any) => {
  return async (id: number, updateData: object) => {
    const vehicle = await employeesVehiclesData.getById(id);

    if (!vehicle) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        person: null,
      };
    }

    const updated = { ...vehicle, ...updateData };
    await employeesVehiclesData.update(updated);

    return { error: null, vehicle: updated };
  };
};
const getBrands = (employeesVehiclesData: { getAllBrands: any; }) => {
  return async () => {

    const brands = await employeesVehiclesData.getAllBrands();

    if (!brands) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        vehicles: null,
      };
    }
    return { error: null, brands };
  }
};
const getModelsAndYears = (employeesVehiclesData: { getAllModelsAndYears: any; }) => {
  return async (brand: string) => {
    const models = await employeesVehiclesData.getAllModelsAndYears(brand);


    if (!models) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        vehicles: null,
      };
    }

    return { error: null, models };
  }
};

const createAVehicle = (employeesData: any) => {
  //to Fix
  return async ({ registration, VIN, car_id, userId }: any) => {
    const existingVehicle = await employeesData.getSingleVehicle(registration);

    if (existingVehicle) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        vehicle: null,
      };
    }
    const vehicle = await employeesData.createVehicle(registration, VIN, car_id, userId)

    return {
      error: null,
      vehicle
    };
  };

};
const filterVehicleByUser = (customersServicesData: { getVehiclesByUser: (arg0: string) => any; }) => {
  return async (userName: string) => {

    const vehicles = await customersServicesData.getVehiclesByUser(userName);

    if (!vehicles) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        vehicles: null,
      };
    }
    return { error: null, vehicles };
  }
};
const getServices = (employeesServicesData: { getAllServices: any; }) => {
  return async () => {

    const services = await employeesServicesData.getAllServices();

    if (!services) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        services: null,
      };
    }
    return { error: null, services };
  }
};
const createAService = (employeesServicesData: any) => {
  //to Fix
  return async (serviceData: any) => {
    const {
      price, service
    } = serviceData;

    const existingService = await employeesServicesData.getServiceByName(service);

    if (existingService[0]) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        service: null,
      };
    }

    return {
      error: null,
      service: await employeesServicesData.create( price, service),
    };
  };
};

const updateService = (employeesServicesData: any) => {
  return async (id: number, updateData: object) => {

    const service = await employeesServicesData.getServiceById(id);

    if (!service) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        service: null,
      };
    }
    const updated = { ...service, ...updateData };

    await employeesServicesData.update(updated);

    return { error: null, service: updated };
  };
};

const removeService = (employeesServicesData: any) => {
  return async (id: number) => {
    const serviceToDelete = await employeesServicesData.getServiceById(id);

    if (!serviceToDelete) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        service: null,
      };
    }

    await employeesServicesData.remove(serviceToDelete);

    return { error: null, service: serviceToDelete };
  };
};

const filteerServiceByName = (employeesServicesData: { getServiceByName: any; }) => {
  return async (serviceName: string) => {
    const services = await employeesServicesData.getServiceByName(serviceName);

   return { error: null, services };
  }
};

const filterByPrice = (employeesServicesData: { getPrice: any; }) => {

  return async (minValue: any, maxValue: any) => {
    maxValue = minValue.slice(1)
    if (!maxValue) {
      const services = await employeesServicesData.getPrice(minValue[0], 100000);
      return { error: null, services };
    }
    const services = await employeesServicesData.getPrice(minValue[0], maxValue);

    if (!services[0]) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        services: null,
      };
    }
    return { error: null, services };
  }
};

const getUserId = (employeesVehiclesData: { getUserIdByName: any; }) => {
  return async (username: string) => {
    const userId = await employeesVehiclesData.getUserIdByName(username);

    if (!userId) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        vehicles: null,
      };
    }

    return { error: null, id: userId };
  }
};

const getAServiceId =  (employeesServicesData: { getServiceId: any; }) => {
  return async (name: string) => {
    const serviceId = await employeesServicesData.getServiceId(name);
  
    if (!serviceId[0].id) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        service: null,
      };
    }

    return { error: null, serviceId: serviceId[0].id };
  }
};

const getAVehicleIdByRegNumber =  (employeesServicesData: { getVehicleIdByRegNumber: any; }) => {
  return async (name: string) => {
    const vehicleId = await employeesServicesData.getVehicleIdByRegNumber(name);
  console.log(vehicleId, 'id')
    if (!vehicleId.id) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        service: null,
      };
    }

    return { error: null, vehicleId: vehicleId.id };
  }
};

const getUserIdServices = (employeesServicesData: { getUserIdByName: any; }) => {
  return async (name: string) => {
    const userId = await employeesServicesData.getUserIdByName(name);

    if (!userId) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        users: null,
      };
    }

    return { error: null, userId:userId };
  }
};
export const employeesServices: any = {
  getAllCars,
  getUserId,
  getVehicles,
  getVehicleReport,
  updateVehicle,
  getBrands,
  getModelsAndYears,
  createAVehicle,
  filterVehicleByUser,
  getServices,
  createAService,
  updateService,
  removeService,
  filteerServiceByName,
  filterByPrice,
  getAServiceId,
  getAVehicleIdByRegNumber,
  getUserIdServices
}