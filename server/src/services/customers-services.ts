import serviceErrors from './service-errors';

const getUserById = (customersData: customersDataFunctions) => {
  return async (id: string) => {
    const user = await customersData.getCustomerById(id);

    if (!user) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }

    return { error: null, user };
  };
};

const getCustomerVehicles = (customersData: customersDataFunctions) => {
  return async (customerId: string) => {
    const vehicles = await customersData.getVehiclesByCustomer(customerId);

    if (!vehicles) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        vehicles: null,
      };
    }

    return { error: null, vehicles };
  };
};

const getAllShopVisits = (customersData: customersDataFunctions) => {
  return async () => {
    return await customersData.getShopVisits();
  }
}

const getShopVisitsByCustomer = (customersData: customersDataFunctions) => {
  return async (id: string) => {
    const visits = await customersData.getShopVisitsByUser(id);

    if (!visits) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        visits: null,
      };
    }

    return { error: null, visits };
  };
};

const createShopVisit = (customersData: customersDataFunctions) => {
  return async (customerId: string, vehicleId: string, serviceId: string, price: string) => {
    const visits = await customersData.createVisit(customerId, vehicleId, serviceId, price);

    return { visits };
  };
};

const advanceShopVisit = (customersData: customersDataFunctions) => {
  return async (customerId: string, visitId: string) => {
    const visits = await customersData.getShopVisitsByUser(customerId);
    const visit = visits.filter((visit: {
      id: string, customerId: string, customer: string, service: string,
      price: string, status: string, start_date: string, end_date: string,
      brand: string, model: string, years: string,
      VIN: string, registration: string
    }) => +visit.id === +visitId)

    if (visit.length === 0) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        visits: null,
      };
    }

    if (visit[0].status === 'Not Started') {
      await customersData.updateStatusStart(visitId);
    }
    if (visit[0].status === 'In Progress') {
      await customersData.updateStatusFinish(visitId);
    }

    return { error: null };
  };
}

const getAllServices = (customersData: customersDataFunctions) => {
  return async () => {
    return await customersData.getServices();
  }
};

const getServices = (customersServicesData: customersDataFunctions) => {
  return async (vehicleId: number, sort?: string, from?: string, to?: string) => {
    const services = await customersServicesData.getAllServicesByCar(vehicleId, sort, from, to);

    if (!services) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        services: null,
      };
    }

    return { error: null, services };
  }
};

// const sortServicesByDate = (customersData: { getAllServicesByCarAndDate: (arg0: number, arg1: string) => any; }) => {
//   return async (carId:number, sort:string) => {
//     const services = await customersData.getAllServicesByCarAndDate(carId, sort);

//     if (!services) {
//       return {
//         error: serviceErrors.RECORD_NOT_FOUND,
//         services: null,
//       };
//     }
//     return { error: null, services };
// }};
const getCustomerCars = (customersServicesData: { getVehiclesByCustomer: (arg0: number) => any; }) => {
  return async (customerId:number) => {
    const cars = await customersServicesData.getVehiclesByCustomer(customerId);

    if (!cars) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        cars: null,
      };
    }
    return { error: null, cars };
  };
};



const filterServicesByDate = (customersData: customersDataFunctions) => {
  return async (carId: number, sort: any, from: any, to: any) => {
    const services = await customersData.getFilteredAndSortedServicesByDate(carId, sort, from, to);

    if (!services) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        services: null,
      };
    }

    return { error: null, services };
  }
};

const getDetailedReport = (customersData: customersDataFunctions) => {
  return async (carId: number) => {
    const services = await customersData.getServicesDetailedReport(carId);

    if (!services) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        services: null,
      };
    }

    return { error: null, services };
  }
};

export default {
  getUserById,
  getCustomerVehicles,
  getAllShopVisits,
  getShopVisitsByCustomer,
  createShopVisit,
  getAllServices,
  advanceShopVisit,
  getCustomerCars,
  getServices,
  filterServicesByDate,
  getDetailedReport,
}

// These probably should be marked as something more than just 'Function'
interface customersDataFunctions {
  getCustomerById: Function,
  getVehiclesByCustomer: Function,
  getShopVisits: Function,
  getShopVisitsByUser: Function,
  createVisit: Function,
  getServices: Function,
  updateStatusStart: Function,
  updateStatusFinish: Function,



  getAllServicesByCar: Function,
  getAllServicesByCarAndDate: Function,
  getFilteredAndSortedServicesByDate: Function,
  getServicesDetailedReport: Function,
}