import { Router } from 'express';
import { authenticationMiddleware, roleMiddleware } from '../auth/auth-middleware';
import { employeesServicesData } from '../data/employees-services-data'
import { employeesServices } from '../services/employees-services';
import serviceErrors from '../services/service-errors';
import servicesSchema from '../validations/schemas/services-schema';
import createValidator from '../validations/validator';

const router = Router();
router

  .get('/',
    authenticationMiddleware, roleMiddleware('employee'),
    async (req, res) => {
      const { search } = req.query;
      const { range } = req.query;

      const { services } = await employeesServices.filteerServiceByName(employeesServicesData)(search);

      res
        .status(200)
        .json(services);

    }
  )

  .post('/',
    authenticationMiddleware, roleMiddleware('employee'),
    [createValidator(servicesSchema)],
    async (req: Request, res: any) => {
      console.log(req.body, 'body')
      //   const {serviceId} = await employeesServices.getAServiceId(employeesServicesData)(req.body.services)
      //  const {vehicleId} = await employeesServices.getAVehicleIdByRegNumber(employeesServicesData)(req.body.registration)
      // const {userId}= await employeesServices.getUserIdServices(employeesServicesData)(req.body.name)

      const { error, service } = await employeesServices.createAService(employeesServicesData)({
        ...req.body,
      });
      if (error === serviceErrors.DUPLICATE_RECORD) {
        res.status(409).send({ message: 'Service already existing' });
      } else {

        res.status(201).send(service);
      }

    })
  .put('/:serviceId',
    authenticationMiddleware, roleMiddleware('employee'),
    [createValidator(servicesSchema)],
    async (req: any, res: any) => {
      const serviceId = req.params.serviceId;
      const updatedService = req.body;

      const { error, service } = await employeesServices.updateService(employeesServicesData)(+serviceId, updatedService);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Service not found!' });
      } else {
        res.status(200).send(service);
      }
    })
  // [...authMiddleware, roleMiddleware('admin')]
  .delete('/:id',
    authenticationMiddleware, roleMiddleware('employee'),
    async (req, res) => {
      const { id } = req.params;

      const { error, service } = await employeesServices.removeService(employeesServicesData)(+id);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Service not found!' });
      } else {
        res.status(200).send(service);
      }
    })


export default router;