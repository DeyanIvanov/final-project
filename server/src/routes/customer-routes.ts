import { Router, Request, Response } from 'express';
import { authenticationMiddleware, identityMiddleware, roleMiddleware } from '../auth/auth-middleware';

import validator from '../validations/validator';
import createShopVisitSchema from '../validations/schemas/create-shop-visit'

import serviceErrors from '../services/service-errors';
import customersData from '../data/customers-queries';
import customersServices from '../services/customers-services';

const router = Router();

router

  // Get all vehicles of a customer
  .get('/:id/vehicles',
    authenticationMiddleware,
    identityMiddleware,
    async (req, res) => {
      const { id } = req.params;

      const { error } = await customersServices.getUserById(customersData)(id);
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res
          .status(404)
          .send({ errorMessage: 'No user with that id.' });
      } else {
        const { vehicles } = await customersServices.getCustomerVehicles(customersData)(id);
        res
          .status(201)
          .json(vehicles);
      }
    })

  // Get for all history customers ( only employees can do that )
  .get('/shop-visits',
    authenticationMiddleware,
    roleMiddleware('employee'),
    async (req, res) => {
      const AllShopVisits = await customersServices.getAllShopVisits(customersData)();

      res
        .status(200)
        .json(AllShopVisits);
    })

  // Get all Services by User Id:
  .get('/:id/shop-visits',
    authenticationMiddleware,
    identityMiddleware,
    async (req: Request, res: Response) => {
      const { id } = req.params;

      const { error } = await customersServices.getUserById(customersData)(id);
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res
          .status(409)
          .send({ errorMessage: 'No user with that id.' });
      } else {
        const { visits } = await customersServices.getShopVisitsByCustomer(customersData)(id);
        res
          .status(200)
          .send({ visits });
      }
    })

  // Create new shop-visit:
  .post('/:id/shop-visits',
    // authenticationMiddleware,
    // identityMiddleware,
    validator(createShopVisitSchema),
    async (req: Request, res: Response) => {
      // check if user exists / has that vehicle:
      const { id } = req.params;

      const { error } = await customersServices.getUserById(customersData)(id);
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res
          .status(404)
          .send({ errorMessage: 'No user with that id.' });
      } else {
        const { vehicles } = await customersServices.getCustomerVehicles(customersData)(id);
        
        if (!vehicles.some((vehicle: { userId: number, id: number, brand: string, model: string, years: string }) =>
          +vehicle.id === +req.body.vehicleId)) {
          res
            .status(404)
            .send({ errorMessage: 'User has no vehicle with that id.' });
        } else {
          const { body } = req;
          await customersServices.createShopVisit(customersData)(id, body.vehicleId, body.serviceId, body.price);

          res
            .status(200)
            .send({ message: 'Shop visit created.' });
        }
      }
    })

  // Advance status of a shop visit (Not Started -> In Progress -> Ready For Pickup):
  .put('/:id/shop-visits/:visitId',
    authenticationMiddleware,
    roleMiddleware('employee'),
    async (req: Request, res: Response) => {
      const { id } = req.params;

      const { error } = await customersServices.getUserById(customersData)(id);
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res
          .status(409)
          .send({ errorMessage: 'No user with that id.' });
      } else {
        const { visitId } = req.params;

        const { error } = await customersServices.advanceShopVisit(customersData)(id, visitId);
        if (error === serviceErrors.RECORD_NOT_FOUND) {
          res
            .status(409)
            .send({ errorMessage: 'User has no shop visit with that id.' });
        } else {
          res
            .status(200)
            .send({ message: 'Shop visit status updated.' });
        }
      }
    })

  //Get services:
  .get('/services',
    authenticationMiddleware,
    async (req: Request, res: Response) => {
      const allServices = await customersServices.getAllServices(customersData)();

      res
        .status(200)
        .json(allServices);
    })
  //get all cars by customer:
  .get('/cars', authenticationMiddleware, roleMiddleware('customer'), async (req: any, res) => {
    const { error, cars } = await customersServices.getCustomerCars(customersData)(req.user?.id);
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).send({ message: 'No cars found' });
    };

    return res
      .status(200)
      .json(cars);
  })

  .get('/cars/:carId/services/detailed', async (req, res) => {
    const carId = +req.params.carId;

    const { error, services } = await customersServices.getDetailedReport(customersData)(carId);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).send({ message: 'No services found' });
    };

    return res
      .status(200)
      .json(services);
  })

  .get('/cars/:carId/services', 
  authenticationMiddleware,
  roleMiddleware('customer'),
  async (req, res) => {
    const vehicleId = +req.params.carId;

    const { sort, from, to } = req.query;
    const { error, services } = await customersServices.getServices(customersData)(vehicleId, sort as string, from as string, to as string);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).send({ message: 'No services found' });
    };

    return res
      .status(201)
      .json(services);
  });

export default router;