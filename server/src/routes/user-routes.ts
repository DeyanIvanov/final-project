import express, { Request, Response } from 'express';
import multer from 'multer';
import path from 'path';
import bcrypt from 'bcrypt';

import { authenticationMiddleware, roleMiddleware } from '../auth/auth-middleware';
import createToken from '../auth/create-token';
import { TOKEN_LIFETIME } from '../config';

import validator from '../validations/validator';
import createUserSchema from '../validations/schemas/create-user';
import loginSchema from '../validations/schemas/login';
import updateUserSchema from '../validations/schemas/update-user';

import serviceErrors from '../services/service-errors';
import usersServices from '../services/users-services';
import usersData from '../data/users-queries';

// upload files
const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'avatars');
  },
  filename(req, file, cb) {
    const filename = Date.now() + path.extname(file.originalname);

    cb(null, filename);
  },
});

const upload = multer({ storage });


const router = express.Router();

router
  /**Public Part
   * 
   * Register:
   * Takes in only an email from the body,
   * then generates the username and random password
   * and sends it to the email that was passed.
  */
  .post('/register',
    validator(createUserSchema),
    async (req: Request, res: Response) => {
      const { email } = req.body;

      const { error } = await usersServices.createUser(usersData)(email);

      if (error === serviceErrors.DUPLICATE_RECORD) {
        res
          .status(409)
          .json({ errorMessage: 'Email not available.' });
      } else {
        res
          .status(201)
          .json({ message: 'Registered Successfully, Check your Email!' });
      }
    })

  // Login:
  .post('/login',
    validator(loginSchema),
    async (req: Request, res: Response) => {
      const { name, password } = req.body;

      const { error, user } = await usersServices.signInUser(usersData)(name, password);

      if (error === serviceErrors.INVALID_SIGNIN) {
        res
          .status(400)
          .send({
            errorMessage: 'Invalid Name / Password',
          });
      } else if (error === serviceErrors.BANNED_USER) {
        res
          .status(400)
          .send({
            errorMessage: 'User is Banned!',
          });
      } else {
        const payload = {
          id: user.id,
          role: user.role,
        };
        const token = createToken(payload, { expiresIn: TOKEN_LIFETIME });

        res
          .status(200)
          .send({
            token,
          });
      }
    })

  // Forgotten Password
  // sends email to the email passed with link to reset form form
  .post('/forgot-password',
    validator(createUserSchema),
    async (req: Request, res: Response) => {
      const { email } = req.body;

      const { error } = await usersServices.forgottenPass(usersData)(email);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res
          .status(404)
          .json({ errorMessage: 'There is no user with that email.' });
      } else {
        res
          .status(200)
          .json({ message: 'Done. Check your email' });
      }
    })

  /**Private Part
   * 
   * Get All Users:
  */
  .get('/',
    authenticationMiddleware,
    async (req: Request, res: Response) => {
      const search: any = req.query.search; // I've marked 'search' as any, because I have validation later on, and it won't cause problems.
      const users = await usersServices.getAllUsers(usersData)(search);

      res
        .status(200)
        .json(users);
    })

  // Get Single User:
  .get('/:id',
    authenticationMiddleware,
    async (req: Request, res: Response) => {
      const { id } = req.params;
      const { error, user } = await usersServices.getUserById(usersData)(id);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res
          .status(409)
          .send({ errorMessage: 'No user with that id!' });
      } else {
        res
          .status(201)
          .send(user);
      }
    })

  // Logout:
  .delete('/logout',
    authenticationMiddleware,
    async (req: Request, res: Response) => {
      const userId = (req?.user as any)?.id;
      const token = req.headers.authorization!.replace('Bearer', '');

      await usersServices.logOutUser(usersData)(token, userId);
      res
        .status(200)
        .json({ message: 'Successfully logged out!' });
    })

  // Ban User by Id:
  .post('/:id/ban',
    authenticationMiddleware,
    roleMiddleware('employee'),
    async (req: Request, res: Response) => {
      const { id } = req.params;
      const { error } = await usersServices.banUser(usersData)(+id);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        return res
          .status(404)
          .send({ errorMessage: 'Invalid user id!' });
      } else {
        res
          .status(200)
          .json({ message: 'User banned!' });
      }
    })

  // Un-ban User by Id:
  .post('/:id/unban',
    authenticationMiddleware,
    roleMiddleware('employee'),
    async (req: Request, res: Response) => {
      const { id } = req.params;
      const { error } = await usersServices.unBanUser(usersData)(+id);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        return res
          .status(404)
          .send({ errorMessage: 'Invalid user id!' });
      } else {
        res
          .status(200)
          .json({ message: 'User unbanned!' });
      }
    })

  // Delete User by Id:
  .delete('/:id',
    authenticationMiddleware,
    roleMiddleware('employee'),
    async (req: Request, res: Response) => {
      const { id } = req.params;
      const { error } = await usersServices.deleteUser(usersData)(+id);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res
          .status(400)
          .send({ errorMessage: 'User not found.' });
      } else {
        res
          .status(200)
          .json({ message: 'Successfully deleted!' });
      }
    })

  // update user
  .put('/:id/update',
    authenticationMiddleware,
    validator(updateUserSchema),
    async (req: Request, res: Response) => {
      const { id } = req.params;

      const body = {
        name: req.body.name || null,
        password: req.body.password || null,
        phone: req.body.phone || null
      };

      const { error } = await usersServices.getUserById(usersData)(id);
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res
          .status(409)
          .send({ errorMessage: 'No user with that id!' });
      }

      let user: object = {};
      if (body.name) {
        user = await usersServices.updateUser(usersData)(id, 'name', body.name);
      }
      if (body.password) {
        const passwordHash = await bcrypt.hash(body.password, 10)
        user = await usersServices.updateUser(usersData)(id, 'password', passwordHash);
      }
      if (body.phone) {
        user = await usersServices.updateUser(usersData)(id, 'phone', body.phone);
      }
      res
        .status(201)
        .json({ ...user });
    })

  // upload avatar picture
  .post('/:id/img',
    authenticationMiddleware,
    upload.single('image'),
    async (req: Request, res: Response) => {
      const { id } = req.params;
      const { error } = await usersServices.uploadAvatar(usersData)(req.file.filename, id);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res
          .status(409)
          .send({ errorMessage: 'No user with that id!' });
      }
      res
        .status(200)
        .json({ image: `avatars/${req.file.filename}` });
    });

export default router;
