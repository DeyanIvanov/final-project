import { Router } from 'express';

import { employeesServices } from '../services/employees-services';
import { employeesServicesData } from '../data/employees-services-data'
import { employeesVehiclesData } from '../data/employees-vehicles-data';
import serviceErrors from '../services/service-errors';

const router = Router();
router

  // Get every car from the db:
  .get('/cars', async (req, res) => {
    const cars = await employeesServices.getAllCars(employeesVehiclesData)();

    return res
      .status(201)
      .json(cars);
  })

  .get('/services', async (req, res) => {
    const { error, services } = await employeesServices.getServices(employeesServicesData)();


    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).send({ message: 'No services found' });
    };

    return res
      .status(201)
      .json(services);
  })

  .get('/services', async (req, res) => {
    const { search } = req.query;

    const { error, services } = await employeesServices.filterServiceByName(employeesServicesData)(search);
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).send({ message: 'No services found' });
    };
    res
      .status(200)
      .json(services);
  })

  .get('/services', async (req, res) => {
    const { range } = req.query;

    const { error, services } = await employeesServices.filterByPrice(employeesServicesData)(range);
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).send({ message: 'No services found' });
    };
    res
      .status(200)
      .json(services);
  })//[createValidator(vehicleSchema)]

  .post('/services',
    //to fix
    async (req, res) => {
      const createdService = req.body;

      const { error, service } = await employeesServices.createAService(employeesServicesData)(createdService);
      if (error === serviceErrors.DUPLICATE_RECORD) {
        res.status(409).send({ message: 'Service already existing' });
      } else {

        res.status(201).send(service);
      }

    })//[createValidator(vehicleSchema)]

  .put('/services/:serviceId',
    //!!to fix
    async (req: any, res: any) => {
      const serviceId = req.params.serviceId;
      const updatedService = req.body;

      const { error, service } = await employeesServices.updateService(employeesServicesData)(+serviceId, updatedService);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        res.status(404).send({ message: 'Service not found!' });
      } else {
        res.status(200).send(service);
      }
    })
  // [...authMiddleware, roleMiddleware('admin')]

  .delete('/services/:id', async (req, res) => {
    const { id } = req.params;
    const { error } = await employeesServices.removeService(employeesServicesData)(+id);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      res.status(404).send({ message: 'Service not found!' });
    } else {
      res.status(200).send({ message: 'Successfully deleted' });
    }
  })

  // -------------------------------------------------------------------------------------------------

  .get('/vehicles', async (req, res) => {
    const { search } = req.query
    if (search) {
      const { error, vehicles } = await employeesServices.filterVehicleByUser(employeesVehiclesData)(search);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        return res.status(404).send({ message: 'No vehicles found' });
      };

      return res
        .status(201)
        .json(vehicles);
    } else {

      const { error, vehicles } = await employeesServices.getVehicles(employeesVehiclesData)();

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        return res.status(404).send({ message: 'No vehicles found' });
      };
console.log(vehicles)
      return res
        .status(201)
        .json(vehicles);
    }
  })

  .get('/vehicles/:vehicleId/report', async (req, res) => {
    const vehicleId = req.params.vehicleId;
    const { error, vehicle } = await employeesServices.getSingleVehicle(employeesVehiclesData)(vehicleId);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).send({ message: 'No vehicles found' });
    };

    return res
      .status(201)
      .json(vehicle);
  })

  // [createValidator(vehicleSchema)]
  .put('/vehicles/:vehicleId/report',
    //!!to fix
    async (req: any, res: any) => {
      const vehicleId = req.params.vehicleId;
      const updatedVehicle = req.body;
      const { error: errorUserName, id: userId } = await employeesServices.getUserId(employeesVehiclesData)(updatedVehicle.name);

      if (errorUserName) {
        return res.status(404).send({ message: 'Username does not exist' });
      }

      const { error, vehicle } = await employeesServices.updateVehicle(employeesVehiclesData)(+vehicleId, {
        ...updatedVehicle,
        users_id: userId,
      });

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        return res.status(404).send({ message: 'Vehicle not found!' });
      }

      return res.status(200).send(vehicle);
    })

  .get('/vehicles/brands', async (req, res) => {
    const { error, brands } = await employeesServices.getBrands(employeesVehiclesData)();


    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).send({ message: 'No brands found' });
    };

    return res
      .status(201)
      .json(brands);
  })

  .get('/vehicles/:brand/modelYears', async (req, res) => {
    const brand = req.params.brand

    const { error, models } = await employeesServices.getModelsAndYears(employeesVehiclesData)(brand);

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).send({ message: 'No models found' });
    };

    return res
      .status(201)
      .json(models);
  })

  .post('/vehicles',
    // [createValidator(vehicleSchema)],
    //to fix
    async (req: any, res: any) => {
      const { name } = req.body;
      const { error: errorUserName, id } = await employeesServices.getUserId(employeesVehiclesData)(name);

      if (errorUserName) {
        return res.status(404).send({ message: 'Username does not exist' });
      }

      const { error, vehicle } = await employeesServices.createAVehicle(employeesVehiclesData)({
        ...req.body,
        userId: id,
      });

      if (error === serviceErrors.DUPLICATE_RECORD) {
        return res.status(409).send({ message: 'Vehicle already exists' });
      }

      const createdVehicle = await employeesVehiclesData.getById(vehicle.id);

      return res.status(201).send(createdVehicle);
    })


export default router;
