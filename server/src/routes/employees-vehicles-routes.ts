import { Router } from 'express';
import { employeesVehiclesData } from '../data/employees-vehicles-data';
import { employeesServices } from '../services/employees-services';
import serviceErrors from '../services/service-errors';
import createValidator from '../validations/validator';
import vehicleSchema from '../validations/schemas/vehicle-schema';
import { authenticationMiddleware, roleMiddleware } from '../auth/auth-middleware';

const router = Router();
router

  .get('/',
    authenticationMiddleware, roleMiddleware('employee'),
    async (req, res) => {
      const { search } = req.query
      if (search) {
        const { error, vehicles } = await employeesServices.filterVehicleByUser(employeesVehiclesData)(search);

        if (error === serviceErrors.RECORD_NOT_FOUND) {
          return res.status(404).send({ message: 'No vehicles found' });
        };

        return res
          .status(201)
          .json(vehicles);
      } else {

        const { error, vehicles } = await employeesServices.getVehicles(employeesVehiclesData)();

        if (error === serviceErrors.RECORD_NOT_FOUND) {
          return res.status(404).send({ message: 'No vehicles found' });
        };

        return res
          .status(201)
          .json(vehicles);
      }
    })
  .get('/:vehicleId/report',
    authenticationMiddleware, roleMiddleware('employee'),
    async (req, res) => {
      const vehicleId = req.params.vehicleId;
      const { error, vehicle } = await employeesServices.getVehicleReport(employeesVehiclesData)(vehicleId);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        return res.status(404).send({ message: 'No vehicles found' });
      };

      return res
        .status(201)
        .json(vehicle);
    })

  .put('/:vehicleId/report',
    authenticationMiddleware,
    roleMiddleware('employee'),
    createValidator(vehicleSchema(false)),
    async (req: any, res: any) => {
      const vehicleId = req.params.vehicleId;
      const { username, ...updatedVehicle } = req.body;

      const { error: errorUserName, id: userId } = await employeesServices.getUserId(employeesVehiclesData)(username);

      if (errorUserName) {
        return res.status(404).send({ message: 'Username does not exist' });
      }

      const { error, vehicle } = await employeesServices.updateVehicle(employeesVehiclesData)(+vehicleId, {
        ...updatedVehicle,
        users_id: userId,
      });

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        return res.status(404).send({ message: 'Vehicle not found!' });
      }

      return res.status(200).send(vehicle);
    })
  .get('/cars', async (req, res) => {
    const cars = await employeesServices.getAllCars(employeesVehiclesData)();

    return res
      .status(201)
      .json(cars);
  })
  .get('/brands',
    authenticationMiddleware, roleMiddleware('employee'),
    async (req, res) => {
      const { error, brands } = await employeesServices.getBrands(employeesVehiclesData)();


      if (error === serviceErrors.RECORD_NOT_FOUND) {
        return res.status(404).send({ message: 'No brands found' });
      };

      return res
        .status(201)
        .json(brands);
    })
  .get('/:brand/modelYears',
    authenticationMiddleware, roleMiddleware('employee'),
    async (req, res) => {
      const brand = req.params.brand

      const { error, models } = await employeesServices.getModelsAndYears(employeesVehiclesData)(brand);

      if (error === serviceErrors.RECORD_NOT_FOUND) {
        return res.status(404).send({ message: 'No models found' });
      };

      return res
        .status(201)
        .json(models);
    })
  .post('/',
    authenticationMiddleware,
    roleMiddleware('employee'),
    createValidator(vehicleSchema(true)),
    async (req: any, res: any) => {
      const { username, ...newVehicle } = req.body;
      const { error: errorUserName, id } = await employeesServices.getUserId(employeesVehiclesData)(username);

      if (errorUserName) {
        return res.status(404).send({ message: 'Username does not exist' });
      }

      const { error, vehicle } = await employeesServices.createAVehicle(employeesVehiclesData)({
        ...newVehicle,
        userId: id,
      });

      if (error === serviceErrors.DUPLICATE_RECORD) {
        return res.status(409).send({ message: 'Vehicle already exists' });
      }

      const createdVehicle = await employeesVehiclesData.getById(vehicle.id);

      return res.status(201).send(createdVehicle);
    })
    .post('/create',
    // [createValidator(vehicleSchema)],
    //to fix
    async (req: any, res: any) => {
      const { name } = req.body;
      const { error: errorUserName, id } = await employeesServices.getUserId(employeesVehiclesData)(name);

      if (errorUserName) {
        return res.status(404).send({ message: 'Username does not exist' });
      }

      const { error, vehicle } = await employeesServices.createAVehicle(employeesVehiclesData)({
        ...req.body,
        userId: id,
      });

      if (error === serviceErrors.DUPLICATE_RECORD) {
        return res.status(409).send({ message: 'Vehicle already exists' });
      }

      const createdVehicle = await employeesVehiclesData.getById(vehicle.id);

      return res.status(201).send(createdVehicle);
    })


export default router;